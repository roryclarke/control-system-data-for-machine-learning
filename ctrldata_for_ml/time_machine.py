"""Handle time information for control system data.

Convenient python module based on :class:`pandas.Timestamp` to handle
time information for data retrieval, batching and re-sampling of control system data.
"""
from __future__ import annotations

import collections
import random

import dateutil.tz  # type: ignore
import numpy
import pandas  # type: ignore


class TimeMachine:
    """Handle time information for control system data.

    :param timezone: local timezone
        e.g. "Europe/Stockholm". None for system timezone,
        defaults to None
    :type timezone: str, optional
    :param begin: Specifies Lower time limit for initial batch span,
        defaults to "1 hour".
    :type begin: str, optional
    :param end: Specifies upper time limit for initial batch span,
        defaults to "None".
    :type end: str, optional
    :param sample_period: Sample period (bin width) for initial batch span.
        defaults to "1 minute"
    :type sample_period: str, optional

    >>> time_machine = TimeMachine(
    ... timezone="Europe/Stockholm", begin="2022-01-01", end = "8 hour"
    ... )
    >>> next(time_machine)
    BatchTimeSpan(2022-01-01 00:00:00+01:00, 2022-01-01 08:00:00+01:00,
        0 days 00:01:00)
    """

    def __init__(
        self,
        timezone: str = None,
        begin: str = "1 hour",
        end: str = None,
        sample_period: str = "1 min",
    ) -> None:
        """Construct class.

        :param timezone: local timezone e.g. "Europe/Stockholm".
            None for system timezone, defaults to None
        :type timezone: str, optional
        :param begin: Specifies Lower time limit for initial batch_span,
            defaults to "1 hour"
        :type begin: str, optional
        :param end: Specifies upper time limit for initial batch_span,
            defaults to None
        :type end: str, optional
        :param sample_period: Sample period (bin width) for initial batch_span,
            defaults to "1 min"
        :type sample_period: str, optional
        """
        if timezone:
            self._timezone = timezone
        else:
            self._timezone = dateutil.tz.gettz()

        self._batch_span = self.str_to_batch_span(begin, end, sample_period)

    def str_to_batch_span(
        self, begin: str = "1 hour", end: str = None, sample_period: str = "1 minute"
    ) -> "BatchTimeSpan":
        """Create batch period from strings for data retrieval and sampling.

        :param begin: Specifies the lower time limit, either absolute as a
            timestamp or relative to `end` as a timedelta.
            Defaults to "1 hour" before `end`.
        :type begin: str, optional
        :param end: Specifies the upper time limit, either absolute as timestamp
            or relative to `begin` as timedelta. None for current time.
            Defaults to None.
        :type end: str, optional
        :param sample_period: Sample period, defaults to "1 minute"
        :type sample_period: str, optional
        :raises ValueError: if batch-span cannot be resolved from input data.
        :return: time span for data retrieval with upper and lower time limits
            floored to the sample period.
        :rtype: BatchTimeSpan

        >>> tm = TimeMachine("Europe/Stockholm")
        >>> tm.str_to_batch_span("2022-01-01","24 h"," 1h")
        BatchTimeSpan(2022-01-01 00:00:00+01:00, 2022-01-02 00:00:00+01:00,
            0 days 01:00:00)
        >>> tm.str_to_batch_span(end="2022-01-01")
        BatchTimeSpan(2021-12-31 23:00:00+01:00, 2022-01-01 00:00:00+01:00,
            0 days 00:01:00)
        """
        _period = TimeMachine.period(sample_period)

        if end is None:
            _end = self.timestamp()
        else:
            _end = self.parse(end)

        _begin = self.parse(begin)

        if isinstance(_begin, pandas.Timestamp):
            if isinstance(_end, pandas.Timedelta):
                _end = _begin + _end
            elif not isinstance(_end, pandas.Timestamp):
                raise ValueError(f"parameter end {end} could not be parsed")
        elif isinstance(_begin, pandas.Timedelta):
            if isinstance(_end, pandas.Timestamp):
                _begin = _end - _begin
            else:
                raise ValueError(
                    f"parameters begin {begin} and end {end} could not be parsed"
                )
        else:
            raise ValueError(f"parameter begin {begin} could not be parsed")

        batch_span = BatchTimeSpan(_begin.floor(_period), _end.floor(_period), _period)
        return batch_span

    @property
    def timezone(self) -> str | dateutil.tz.tz.tzfile:
        """Local timezone. Naive time is localized to this timezone."""
        return self._timezone

    def parse(
        self, time: str
    ) -> pandas.Timedelta | pandas.Timestamp | pandas.NaT | str:
        """Parse a time string into pandas.Timedelta or pandas.Timestamp.

        :param time: timestring to be parsed
        :type time: str
        :raises TypeError: If `time` is not str
        :return: Time parsed by :meth:`timestamp` or :meth:`period`
         with the following exceptions: 'None', 'nan' etc. that could be parse as
         pd.NaT will not be parsed, while 'Never' and 'NaT' will be parsed as
         :class:`pd.NaT` If `time` could not be parsed then `time` is returned.
        :rtype: pandas.Timedelta | pandas.Timestamp | str

        >>> tm = TimeMachine(timezone = "Europe/Stockholm")
        >>> tm.parse('0.1 second')
        Timedelta('0 days 00:00:00.100000')
        >>> tm.parse('2021-10-20T00:00:00')
        Timestamp('2021-10-20 00:00:00+0200', tz='Europe/Stockholm')
        >>> tm.parse('this will not be parsed')
        'this will not be parsed'
        >>> tm.parse("Never")
        NaT
        >>> tm.parse("NaT")
        NaT
        >>> tm.parse(0) # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
        TypeError: 0 is not str
        >>> tm.parse("nan")
        'nan'
        """
        if not isinstance(time, str):
            raise TypeError(f"{time!r} is not str")

        if time in ["Never", "NaT"]:
            return pandas.NaT

        decoders = [self.timestamp, self.period]
        for decoder in decoders:
            try:
                value = decoder(time)  # type: ignore
                if not pandas.isnull(value):
                    return value
            except ValueError:
                pass
        return time

    def timestamp(
        self, arg: None | str | int | pandas.Timestamp = None
    ) -> pandas.Timestamp:
        """Parse timestamp and localize or convert it to local timezone.

        :param arg: time to be parsed. Integer are interpreted as nanoseconds past
         epoch. Strings and Timestamps are interpreted as
         local time if no zone information is given. None for the current time.
         Defaults to None
        :type time: None | str | int | pandas.Timestamp, optional
        :raises TypeError: If `arg` is of incorrect type.
        :return: timestamp in the default timezone
        :rtype: pandas.Timestamp

        >>> time_machine = TimeMachine(timezone = "Europe/Stockholm")
        >>> time_machine.timestamp("Jun/01/2021 17:07:02").isoformat()
        '2021-06-01T17:07:02+02:00'
        >>> time_machine.timestamp(0).isoformat()
        '1970-01-01T01:00:00+01:00'
        >>> timestamp = pandas.Timestamp("1970-01-01T01:00:00")
        >>> time_machine.timestamp(timestamp).isoformat()
        '1970-01-01T01:00:00+01:00'
        >>> time_machine.timestamp("2000-01-01T00:00:00+05:00").isoformat()
        '1999-12-31T20:00:00+01:00'
        >>> time_machine.timestamp() > pandas.Timestamp('2022-01-05 08:42:46+0100')
        True
        """
        if arg is None:
            time = pandas.Timestamp.now()
        else:
            time = pandas.Timestamp(arg)

        if isinstance(arg, int):
            time = time.tz_localize("utc")
            time = time.tz_convert(self.timezone)
            return time

        if arg is None or isinstance(arg, (pandas.Timestamp, str)):
            try:
                time = time.tz_localize(self.timezone)
            except TypeError:
                time = time.tz_convert(self.timezone)
        else:
            raise TypeError(f"{arg} does not have the right type")
        return time

    def short_isoformat(
        self, time: int | str | pandas.Timestamp | pandas.Timedelta | None
    ) -> str:
        """Create a short alphanumerical timestamp on isoformat.

        :param time: time to parse
        :type time: int | str | pandas.Timestamp | pandas.Timedelta | None
        :raises ValueError: If time is not defined.
        :return: short alphanumerical timestamp without timezone.
        :rtype: str
        """
        try:
            value = self.timestamp(time)  # type: ignore
            if pandas.isnull(value):
                raise ValueError(f"{time} is not defined")
            naive = value.tz_localize(None)
            timestamp: str = naive.isoformat()
            short = timestamp.replace("-", "").replace(":", "")
            return short
        except (TypeError, ValueError) as type_error:
            value = self.period(time)  # type: ignore
            if pandas.isnull(value):
                raise type_error
            period_stamp = value.isoformat()
            return period_stamp

    @staticmethod
    def period(time: str | pandas.Timedelta | int) -> pandas.Timedelta:
        """Parse period, including EPICS scan fields.

        https://epics.anl.gov/base/R7-0/6-docs/menuScan.html

        :param time: string on format 'value unit', eg. '1 sec', '1 Hertz',
         'P0DT0H0M1S'.
        :type time: str | pandas.Timedelta | int
        :return: parsed time period
        :rtype: pandas.Timedelta

        >>> TimeMachine.period('10 Hertz').isoformat()
        'P0DT0H0M0.1S'
        >>> TimeMachine.period('P0DT0H0M0.1S')
        Timedelta('0 days 00:00:00.100000')
        >>> TimeMachine.period("0.5 s")
        Timedelta('0 days 00:00:00.500000')
        >>> TimeMachine.period("500L")
        Timedelta('0 days 00:00:00.500000')
        >>> TimeMachine.period(1)
        Timedelta('0 days 00:00:00.000000001')

        """
        try:
            _period = pandas.Timedelta(time)
        except ValueError as value_error:
            if isinstance(time, str):
                val_str, unit = time.split(" ")
                val = float(val_str)
                if unit in ["Hz", "Hertz"]:
                    val = 1 / val
                    unit = "seconds"
                _period = pandas.Timedelta(val, unit=unit, errors="raise")
            else:
                raise value_error

        return _period

    def time_frame(
        self,
        data: pandas.DataFrame,
        seconds: str | None = "secs",
        nanoseconds: str | None = "nanos",
    ) -> pandas.DataFrame:
        """Replace seconds and nanoseconds columns in dataset with a timeindex.

        :param data: dataframe with seconds and/or nanoseconds columns
        :type dataframe: pandas.DataFrame
        :param seconds: name of column containing seconds past epoch (utc),
            defaults to "secs"
        :type seconds: str| None, optional
        :param nanoseconds: column name with nanoseconds, defaults to "nanos"
        :type nanoseconds: str | None, optional
        :return: dataframe with `pandas.DatetimeIndex` as time index.
        :rtype: pandas.DataFrame

        >>> time_machine = TimeMachine(
        ...    timezone = "Europe/Stockholm",
        ...    begin = "2021-10-10T07:00:00",
        ...    end = "1 min")
        >>> batch = next(time_machine)
        >>> data = batch.fake_data(average_period = "10 seconds")
        >>> data
                 secs      nanos       val
        0  1633841999  700667087 -0.196676
        1  1633842017  345448085  0.002135
        2  1633842023  461397383 -0.879415
        3  1633842040  601138576  0.397811
        4  1633842050  129341122  0.754252
        5  1633842054  529868248 -1.007353
        >>> time_machine.time_frame(data)
                                                  val
        Time
        2021-10-10 06:59:59.700667087+02:00 -0.196676
        2021-10-10 07:00:17.345448085+02:00  0.002135
        2021-10-10 07:00:23.461397383+02:00 -0.879415
        2021-10-10 07:00:40.601138576+02:00  0.397811
        2021-10-10 07:00:50.129341122+02:00  0.754252
        2021-10-10 07:00:54.529868248+02:00 -1.007353

        """
        _data = data.copy(False)
        if seconds in _data.columns:
            _nanoseconds = _data.pop(seconds) * 1_000_000_000
            if nanoseconds in _data.columns:
                _nanoseconds += _data.pop(nanoseconds)
        elif nanoseconds in _data.columns:
            _nanoseconds = _data.pop(nanoseconds)
        else:
            raise ValueError("No time information in dataframe")

        index = pandas.DatetimeIndex(
            _nanoseconds, name="Time", tz="utc"
        )  # type: ignore
        index = index.tz_convert(tz=self.timezone)  # pylint: disable=no-member
        _data.index = index
        return _data

    def convert_columns(
        self,
        data: pandas.DataFrame,
        seconds: str | None = "secs",
        nanoseconds: str | None = "nanos",
    ) -> pandas.Series:
        """Generate time from a dataframe.

        :param data: dataframe with seconds and/or nanoseconds columns
        :type dataframe: pandas.DataFrame
        :param seconds: name of column containing seconds past epoch (utc),
            defaults to "secs"
        :type seconds: str | None, optional
        :param nanoseconds: Column name with nanoseconds, defaults to "nanos"
        :type nanoseconds: str | None, optional
        :return: timestamps converted from dataframe
        :rtype: pandas.Series

        >>> time_machine = TimeMachine(
        ...    timezone = "Europe/Stockholm",
        ...    begin = "2021-10-10T07:00:00",
        ...    end = "1 min")
        >>> batch = next(time_machine)
        >>> data = batch.fake_data(average_period = "10 seconds")
        >>> data
                 secs      nanos       val
        0  1633841999  700667087 -0.196676
        1  1633842017  345448085  0.002135
        2  1633842023  461397383 -0.879415
        3  1633842040  601138576  0.397811
        4  1633842050  129341122  0.754252
        5  1633842054  529868248 -1.007353
        >>> time_machine.convert_columns(data,"secs","nanos")
        0   2021-10-10 06:59:59.700667087+02:00
        1   2021-10-10 07:00:17.345448085+02:00
        2   2021-10-10 07:00:23.461397383+02:00
        3   2021-10-10 07:00:40.601138576+02:00
        4   2021-10-10 07:00:50.129341122+02:00
        5   2021-10-10 07:00:54.529868248+02:00
        Name: Time, dtype: datetime64[ns, Europe/Stockholm]
        """
        times: pandas.Series = self.time_frame(
            data, seconds=seconds, nanoseconds=nanoseconds
        ).index.to_series(index=data.index)

        return times

    @staticmethod
    def trim(timestamps: pandas.Series, time_limits: pandas.Series) -> pandas.Series:
        """Trim timestamps to be between time span limits.

        :param timestamps: Timestamps to be trimmed
        :type timestamps: pandas.Series
        :param time_limits: Upper time limits. Lower time limits as shifted.
        :type time_limits: pandas.Series
        :return: Timestamps trimmed between previous time and time in time_limits.
        :rtype: pandas.Series

        >>> time_machine = TimeMachine(
        ...    timezone = "Europe/Stockholm",
        ...    begin = "2021-10-10T07:00:00",
        ...    end = "1 hour")
        >>> batch = next(time_machine)
        >>> data = batch.fake_data(average_period = "1 min")
        >>> time_limits = time_machine.convert_columns(
        ...    data, seconds="secs", nanoseconds=None
        ... )
        >>> right = time_limits[[0, 10, 20, 30]]
        >>> left = time_limits.shift(1)[[0, 10, 20, 30]]
        >>> pandas.concat([left,right], keys = ["left","right"],axis=1)
                                left                     right
        0                        NaT 2021-10-10 06:59:59+02:00
        10 2021-10-10 07:09:08+02:00 2021-10-10 07:09:14+02:00
        20 2021-10-10 07:20:08+02:00 2021-10-10 07:20:54+02:00
        30 2021-10-10 07:28:52+02:00 2021-10-10 07:30:25+02:00
        >>> timestamps = [time_machine.timestamp(0),left[10],left[10], time_limits[59]]
        >>> before = pandas.Series(timestamps, index = right.index)
        >>> after = time_machine.trim(before,time_limits)
        >>> pandas.concat([before,after], keys = ["before", "after"], axis=1)
                              before                     after
        0  1970-01-01 01:00:00+01:00 1970-01-01 01:00:00+01:00
        10 2021-10-10 07:09:08+02:00 2021-10-10 07:09:08+02:00
        20 2021-10-10 07:09:08+02:00 2021-10-10 07:20:08+02:00
        30 2021-10-10 07:59:41+02:00 2021-10-10 07:30:25+02:00
        """
        max_times = time_limits
        times = pandas.concat([timestamps, max_times], axis=1, join="inner").min(axis=1)
        min_times = time_limits.shift(1)
        times = (
            pandas.concat([times, min_times], axis=1, join="inner")
            .fillna(method="ffill", axis=1)
            .max(axis=1)
        )
        return times

    def __iter__(self) -> collections.abc.Iterable["TimeMachine"]:
        """Create an iterator for batch spans.

        :return: Iterator of batch spans
        :rtype: collection.abc.Iterable[TimeMachine]
        """
        return self  # type: ignore

    def __next__(self) -> "BatchTimeSpan":
        """Shift `batch_span` forward in time.

        :return: batch span
        :rtype: BatchTimeSpan

        >>> tm = TimeMachine(
        ...   timezone="Europe/Stockholm",
        ...   begin="2022-01-01",
        ...   end = "24 hour",
        ...   sample_period = "1 hour"
        ... )
        >>> for batch_span in iter(tm):
        ...   print(f'{batch_span.begin} - {batch_span.end}')
        2022-01-01 00:00:00+01:00 - 2022-01-02 00:00:00+01:00
        2022-01-02 00:00:00+01:00 - 2022-01-03 00:00:00+01:00
        2022-01-03 00:00:00+01:00 - 2022-01-04 00:00:00+01:00
        2022-01-04 00:00:00+01:00 - 2022-01-05 00:00:00+01:00
                ...
        """
        current = self._batch_span
        if current.end > self.timestamp():
            raise StopIteration("Upper time limit passed current time")

        self._batch_span = current.next()
        return current


class BatchTimeSpan:
    """Convenient class to handle batch period information for control system data.

    :param begin: Lower time limit for data retrival.
    :type begin: pandas.Timestamp
    :param end: Upper time limit for batch as timestamp, as timedelta after begin.
        Defaults to None, which gives the current time if begin is not a timestamp.
    :type end: pandas.Timestamp
    :param sample_period: Sample period.
    :type sample_period: pandas.Timedelta

    >>> tm = TimeMachine(
    ...    timezone="Europe/Stockholm",begin="2022-01-01", end = "8 hour"
    ... )
    >>> next(tm)
    BatchTimeSpan(2022-01-01 00:00:00+01:00, 2022-01-01 08:00:00+01:00,
        0 days 00:01:00)

    """

    def __init__(
        self,
        begin: pandas.Timestamp,
        end: pandas.Timestamp,
        sample_period: pandas.Timedelta,
    ) -> None:
        """Construct class.

        :param begin: Lower time limit of batch.
        :type begin: pandas.Timestamp
        :param end: Upper time limit of batch.
        :type end: pandas.Timestamp
        :param sample_period: Sampling period within the batch.
        :type sample_period: pandas.Timedelta
        """
        self._sample_period = sample_period
        self._begin = begin
        self._end = end
        self._random = random.Random(43)

    def __repr__(self) -> str:
        """Represent the class."""
        return (
            f"{self.__class__.__name__}({self.begin}, {self.end}, {self.sample_period})"
        )

    @property
    def end(self) -> pandas.Timestamp:
        """Upper time limit of the batch."""
        return self._end

    @property
    def begin(self) -> pandas.Timestamp:
        """Lower time limit of the batch."""
        return self._begin

    @property
    def sample_period(self) -> pandas.Timedelta:
        """Sample period within batch."""
        return self._sample_period

    @property
    def time_index(self) -> pandas.DatetimeIndex:
        """Time index for batch. Time indicate the start of sample periods."""
        times = pandas.date_range(
            self.begin, self.end, freq=self.sample_period, closed="left", name="Time"
        )
        return times

    def fake_data(self, average_period: str = "1 second") -> pandas.DataFrame:
        """Generate faked epics data.

        :param average_period: average time between datapoints, defaults to
            "1 second"
        :type rate: str
        :return: data set with random seconds, nanoseconds and values.
            First data point is intentionally outside of the batch span.
        :rtype: pandas.DataFrame

        >>> time_machine = TimeMachine(
        ...     timezone="Europe/Stockholm",
        ...     begin = "2021-10-10T07:00:00",
        ...     end = "1 min")
        >>> batch_span = next(time_machine)
        >>> batch_span.fake_data(average_period="10 sec")
                 secs      nanos       val
        0  1633841999  700667087 -0.196676
        1  1633842017  345448085  0.002135
        2  1633842023  461397383 -0.879415
        3  1633842040  601138576  0.397811
        4  1633842050  129341122  0.754252
        5  1633842054  529868248 -1.007353
        """
        sample_size = (self.end - self.begin) // TimeMachine.period(average_period)
        nanoseconds_list = [
            self._random.randrange(self.begin.value, self.end.value)
            for n in range(sample_size)
        ]
        nanoseconds = pandas.Series(sorted(nanoseconds_list), dtype=int)
        seconds, nanoseconds = nanoseconds.divmod(1_000_000_000)
        # Move first point outside of span to mimic real data set.
        seconds[0] = self.begin.value // 1_000_000_000 - 1
        val = [self._random.gauss(0, 1) for n in range(sample_size)]
        val = pandas.Series(val)
        data = pandas.concat(
            [seconds, nanoseconds, val], axis=1, keys=["secs", "nanos", "val"]
        )
        return data

    def _add_interval_endpoints(self, data: pandas.Series) -> pandas.Series:
        """Add datapoints at interval endpoints and remove outside of batch span.

        :param data: time series data
        :type data: pandas.Series
        :return: modified time series data
        :rtype: pandas.Series


        >>> time_machine = TimeMachine(
        ...     timezone = "Europe/Stockholm",
        ...     begin = "2021-10-10T07:00:00",
        ...     end = "1 min",
        ...     sample_period = '20 sec')
        >>> batch_span = next(time_machine)
        >>> data = batch_span.fake_data(average_period="10 sec")
        >>> data = time_machine.time_frame(data,"secs","nanos")
        >>> data
                                                  val
        Time
        2021-10-10 06:59:59.700667087+02:00 -0.196676
        2021-10-10 07:00:17.345448085+02:00  0.002135
        2021-10-10 07:00:23.461397383+02:00 -0.879415
        2021-10-10 07:00:40.601138576+02:00  0.397811
        2021-10-10 07:00:50.129341122+02:00  0.754252
        2021-10-10 07:00:54.529868248+02:00 -1.007353

        >>> batch_span._add_interval_endpoints(data["val"])
        Time
        2021-10-10 07:00:00+02:00             -0.196676
        2021-10-10 07:00:17.345448085+02:00    0.002135
        2021-10-10 07:00:20+02:00              0.002135
        2021-10-10 07:00:23.461397383+02:00   -0.879415
        2021-10-10 07:00:40+02:00             -0.879415
        2021-10-10 07:00:40.601138576+02:00    0.397811
        2021-10-10 07:00:50.129341122+02:00    0.754252
        2021-10-10 07:00:54.529868248+02:00   -1.007353
        Name: val, dtype: float64
        """
        no_data = pandas.Series(numpy.nan, index=self.time_index, name=data.name)
        _data = pandas.concat([data, no_data], axis=0, sort=False)
        _data = _data[~_data.index.duplicated(keep="first")]
        _data = _data.sort_index().fillna(method="ffill")
        _data = _data[self.begin : self.end]
        return _data

    def _weights(self, timestamps: pandas.DatetimeIndex) -> pandas.Series:
        """Calculate weights within each sample period.

        :param timestamps: sorted timestamps on the interval [begin,end)
        :type timestamps: pandas.DatetimeIndex
        :return: weights within each bin
        :rtype: pandas.Series

        >>> time_machine = TimeMachine(
        ...     timezone = "Europe/Stockholm",
        ...     begin = "2021-10-10T07:00:00",
        ...     end = "1 min",
        ...     sample_period = '20 sec')
        >>> batch_span = next(time_machine)
        >>> data = batch_span.fake_data(average_period="10 sec")
        >>> data = time_machine.time_frame(data,"secs","nanos")
        >>> data = batch_span._add_interval_endpoints(data["val"])
        >>> weights = batch_span._weights(data.index)
        >>> weights
        Time
        2021-10-10 07:00:00+02:00              0.867272
        2021-10-10 07:00:17.345448085+02:00    0.132728
        2021-10-10 07:00:20+02:00              0.173070
        2021-10-10 07:00:23.461397383+02:00    0.826930
        2021-10-10 07:00:40+02:00              0.030057
        2021-10-10 07:00:40.601138576+02:00    0.476410
        2021-10-10 07:00:50.129341122+02:00    0.220026
        2021-10-10 07:00:54.529868248+02:00    0.273507
        Name: weight, dtype: float64

        >>> weights.resample(batch_span.sample_period).sum()
        Time
        2021-10-10 07:00:00+02:00    1.0
        2021-10-10 07:00:20+02:00    1.0
        2021-10-10 07:00:40+02:00    1.0
        Freq: 20S, Name: weight, dtype: float64
        """
        _timestamps = timestamps.to_series()
        _weights = _timestamps.diff().shift(-1) / self.sample_period
        _weights[-1] = (self.end - _timestamps[-1]) / self.sample_period
        _weights.name = "weight"
        return _weights

    def resample(self, data: pandas.Series) -> pandas.DataFrame:
        """Resample data to a periodic time spans by time average.

        :param data: time series data to resample.
        :type data: pandas.Series
        :return:  time averaged mean and variance.
        :rtype: pandas.DataFrame

        >>> time_machine = TimeMachine(
        ... timezone = "Europe/Stockholm",
        ... begin = "2021-01-01T10",
        ... end = "1 hour",
        ... sample_period = '10 min')
        >>> batch_span = next(time_machine)
        >>> data = batch_span.fake_data(average_period="1 min")
        >>> data = time_machine.time_frame(data,"secs","nanos")
        >>> batch_span.resample(data["val"])
                                    average  variance
        Time
        2021-01-01 10:00:00+01:00 -0.052328  1.350303
        2021-01-01 10:10:00+01:00  0.278885  0.811071
        2021-01-01 10:20:00+01:00 -0.187527  1.152125
        2021-01-01 10:30:00+01:00  0.155158  0.685258
        2021-01-01 10:40:00+01:00  0.475800  1.221839
        2021-01-01 10:50:00+01:00 -0.191343  0.482986

        """
        val = self._add_interval_endpoints(data)

        # differentiate
        average = val * self._weights(val.index)
        variance = val * average

        # integrate over sample periods:
        result = pandas.concat(
            [average, variance], axis=1, keys=["average", "variance"]
        )
        result = result.resample(self.sample_period).sum()
        result["variance"] -= (result["average"]) ** 2

        return result

    def next(self) -> "BatchTimeSpan":
        """Create a new batch time span shifted forward in time.

        :return: timemachine shifted forward in time
        :rtype: BatchTimeSpan

        >>> time_machine = TimeMachine(
        ...   timezone="Europe/Stockholm",
        ...   begin="2022-01-01",
        ...   end = "24 hour",
        ...   sample_period = "1 hour"
        ... )
        >>> batch_span = time_machine._batch_span
        >>> while batch_span.end < time_machine.timestamp():
        ...   print(f'{batch_span.begin} - {batch_span.end}')
        ...   batch_span = batch_span.next()
        2022-01-01 00:00:00+01:00 - 2022-01-02 00:00:00+01:00
        2022-01-02 00:00:00+01:00 - 2022-01-03 00:00:00+01:00
        2022-01-03 00:00:00+01:00 - 2022-01-04 00:00:00+01:00
        2022-01-04 00:00:00+01:00 - 2022-01-05 00:00:00+01:00
            ...
        """
        batch_period = self.end - self.begin
        begin = self.end
        end = begin + batch_period
        next_batch_span = BatchTimeSpan(
            begin=begin, end=end, sample_period=self.sample_period
        )
        return next_batch_span
