"""read data and meta data from EPICS Archiver Appliance."""
from __future__ import annotations

from typing import Any
from urllib import parse

import requests


class ArchiveReader:
    """Read raw data and meta data from EPICS Archiver Appliance.

    Implements GET requests from the
    :archiver:`Management API <api/mgmt_scriptables.html>` and
    :archiver:`User Guide <userguide.html>`. Outputs json object

    :param hostname: EPICS Archiver Appliance hostname
    :type hostname: str
    :param port: EPICS Archiver Appliance management port, defaults to 17665
    :type port: int, optional

    >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
    >>> archiver
    ArchiveReader(archiver-01.tn.esss.lu.se:17665)

    """

    def __init__(self, hostname, port=17665):
        """Construct.

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> archiver
        ArchiveReader(archiver-01.tn.esss.lu.se:17665)

        """
        self.hostname = hostname
        self.port = port
        self.session = requests.Session()

    def __repr__(self):
        """Represent the class."""
        return f"{self.__class__.__name__}({self.hostname}:{self.port})"

    @property
    def _management_url(self) -> str:
        """URL to the business processes pertaining to the management webapp.

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> archiver._management_url
        'http://archiver-01.tn.esss.lu.se:17665/mgmt/bpl/'

        """
        return f"http://{self.hostname}:{self.port}/mgmt/bpl/"

    @property
    def _data_url(self) -> str:
        """EPICS Archiver Appliance data retrieval url.

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> archiver._data_url
        'http://172.16.225.16:17668/retrieval/data/getData.json'

        """
        return self.get_appliance_info()["dataRetrievalURL"] + "/data/getData.json"

    def _get(self, endpoint, **kwargs) -> Any:
        r"""Send a GET request to the given endpoint.

        :param endpoint: API endpoint (relative or absolute)
        :param \*\*kwargs: Optional arguments to be sent
        :return: data
        :rtype: Any
        """
        url = parse.urljoin(self._management_url, endpoint.lstrip("/"))
        response = self.session.request("GET", url, **kwargs)
        response.raise_for_status()
        return response.json()

    def _get_all_aliases(self) -> dict[str, str]:
        """Get all the aliases in the cluster and the PV names they are mapped to.

        Javadoc: :archiver_mgmt_url:`GetAllAliasesAction`
        """
        return self._get("/getAllAliases")

    def get_all_pv_names(
        self, pv_name: str = None, regex: str = None, limit=500
    ) -> list[str]:
        """Get all the PVs in the cluster.

        Javadoc: :archiver_mgmt_url:`GetAllPVs`

        Note this call can return millions of PVs.

        :param pv_name: PV name that can contain a glob wildcard.
        :type pv_name: str, optional
        :param regex: Java regex wildcard, defaults to None
        :type regex: str, optional
        :param limit:  Maximum number of matched PV names return, defaults to 500
        :type limit: int, optional
        :return: All PV names in the cluster that matches the py:param: `pv_name` or
            regex.
        :rtype: list([str])

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> archiver.get_all_pv_names(pv_name = "RFQ*", limit=3)
        ['RFQ-010:AF-CONN', 'RFQ-010:CPU-CONN', 'RFQ-010:Ctrl-IOC-01:ACCESS']

        """
        return self._get(
            "/getAllPVs", params={"pv": pv_name, "regex": regex, "limit": limit}
        )

    def get_appliance_info(self) -> dict[str, str]:
        """Get the appliance information.

        Javadoc: :archiver_mgmt_url:`GetApplianceInfo`

        :return: EPICS Archiver Appliance information
        :rtype: dict

        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se')
        >>> from pprint import pprint
        >>> pprint(archiver.get_appliance_info())
        {'clusterInetPort': '172.16.225.16:16670',
         'dataRetrievalURL': 'http://172.16.225.16:17668/retrieval',
         'engineURL': 'http://172.16.225.16:17666/engine/bpl',
         'etlURL': 'http://172.16.225.16:17667/etl/bpl',
         'identity': 'archiver-01',
         'mgmtURL': 'http://172.16.225.16:17665/mgmt/bpl',
         'retrievalURL': 'http://172.16.225.16:17668/retrieval/bpl',
         'version': 'Archiver Appliance Version '
         '0.0.1_SNAPSHOT_15-November-2018T10-27-25'}

        """
        return self._get("/getApplianceInfo")

    def get_appliances_in_cluster(self) -> list[dict]:
        """Get information for all active appliances in the cluster.

        Javadoc: :archiver_mgmt_url:`GetAppliancesInCluster`

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> appliances = archiver.get_appliances_in_cluster()
        >>> from pprint import pprint
        >>> pprint(appliances)
        [{'clusterInetPort': '172.16.225.16:16670',
          'dataRetrievalURL': 'http://172.16.225.16:17668/retrieval',
          'engineURL': 'http://172.16.225.16:17666/engine/bpl',
          'etlURL': 'http://172.16.225.16:17667/etl/bpl',
          'identity': 'archiver-01',
          'mgmtURL': 'http://172.16.225.16:17665/mgmt/bpl',
          'retrievalURL': 'http://172.16.225.16:17668/retrieval/bpl'},
         {'clusterInetPort': '172.16.225.17:16670',
          'dataRetrievalURL': 'http://172.16.225.17:17668/retrieval',
          'engineURL': 'http://172.16.225.17:17666/engine/bpl',
          'etlURL': 'http://172.16.225.17:17667/etl/bpl',
          'identity': 'archiver-02',
          'mgmtURL': 'http://172.16.225.17:17665/mgmt/bpl',
          'retrievalURL': 'http://172.16.225.17:17668/retrieval/bpl'}]

        """
        return self._get("/getAppliancesInCluster")

    def get_archived_waveform_pvs(self) -> list[dict]:
        """Get waveform PVs that are currently being archived.

        Javadoc: :archiver_mgmt_url:`reports/WaveformPVsAction`

        :return: Waveform PVs that are currently being archived.
        :rtype: list[dict]

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se") # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> waveforms = archiver.get_archived_waveform_pvs() # doctest: +SKIP
        >>> len(waveforms) # doctest: +SKIP
        9971
        >>> pprint(waveforms[0]) # doctest: +SKIP
        {'elementCount': '256',
         'pvName': 'MEBT-010:PBI-BPM-007:SA-HDF1-FileTemplate_RBV',
         'samplingmethod': 'MONITOR',
         'samplingperiod': '1.0'}

        """
        return self._get("/getArchivedWaveforms")

    def get_data(
        self, pv_name: str, begin: str, end: str, **kwargs
    ) -> list[dict[str, dict]]:
        """Get archived data.

        Javadoc: :archiver:`User Guide <userguide.html>`

        :param pv_name: Name of the PV
        :type pv_name: str
        :param begin: start time on isoformat yyyy-mm-ddTHH:MM:SS±HH:MM
            for non UTC and yyyy-mm-ddTHH:MM:SSZ for UTC
        :type begin: str
        :param end: end time on the same format as start time
        :type end: str
        :param fetchLatestMetadata: If "true", an extra call
            s made to the
            engine as part of the retrieval to get the latest values of the various
            fields (DESC, HIHI etc).
        :type fetchLatestMetadata: str, optional
        :param retiredPVTemplate: If specified, the archiving
            information
            (PVTypeInfo) for the PV specified in this parameter is used as a template
            for PVs that do not exist in the system. This is intended principally for
            use with legacy PVs (PVs that no longer exist on the LAN and do not have
            a PVTypeInfo). For example, all the data for legacy PV's could be
            consolidated into yearly partitions and stored on tape. When a user
            places a request for this data, it could be restored to some standard
            folder, for example /arch/tape. A template PV (probably paused), for
            example, TEMPLATE:PV is added to the system with one of its stores
            pointing to /arch/tape. For all data retrieval requests, this template PV
            is specified as the value of the retiredPVTemplate argument.
            Because the archiver does not find a PVTypeInfo for LEGACY:PV, it uses
            the PVTypeInfo for TEMPLATE:PV to determine data stores for the
            LEGACY:PV. The data in /arch/tape is used to fulfill the data retrieval
            request. Once the user is done with the data for LEGACY:PV, the data in
            arch/tape can be deleted.
        :type retiredPVTemplate: str, optional
        :param timeranges: Get data for a sequence of time ranges. Time
            ranges are specified as a comma-separated list if ISO 8601 strings.
        :type timeranges: str, optional
        :param donotchunk: Use this to skip HTTP chunking of the response.
            This is meant for client that do not understand chunked responses.
        :type donotchunk: str, optional
        :param ca_count: This is passed on to an external ChannelArchiver
            as the value of the count parameter in the archiver.values XMLRPC call.
            The limits the number of samples returned from the ChannelArchiver; id
            unspecified, this defaults to 100000. If this is too large, you may
            timeouts from the ChannelArchiver.
        :type ca_count: str, optional
        :param ca_how: This is passed on to an external ChannelArchiver as
            the value of the how parameter in the archiver.values XMLRPC call. This
            defaults to 0; that is, by default, we ask for raw data.
        :type ca_how: str, optional

        :return: dictionary with data and meta-data
        :rtype: list[dict]

        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se')
        >>> from pprint import pprint
        >>> data = archiver.get_data(
        ... 'CWM-CWS03:WtrC-TT-020:Tmp',
        ... begin='2021-11-01T09:00:00+01:00',
        ... end='2021-11-01T09:05:00+01:00',
        ... fetchLatestMetadata = 'true')
        >>> pprint(data[0]["data"])
        [{'nanos': 187731174,
              'secs': 1614157068,
              'severity': 0,
              'status': 0,
              'val': 25.476707458496094}]
        >>> pprint(data[0]["meta"])
        {'PREC': '0', 'name': 'CWM-CWS03:WtrC-TT-020:Tmp'}

        """
        params = {
            "pv": pv_name,
            "from": begin,
            "to": end,
        }
        params.update(kwargs)
        data = self._get(self._data_url, params=params)
        return data

    def get_disconnected_pvs(self) -> list[dict]:
        """Get PVs that are currently disconnected.

        Javadoc: :archiver_mgmt_url:`reports/CurrentlyDisconnectedPVs`

        :return: PVs that are currently disconnected.
        :rtype: list[dict]

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se") # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> pvs = archiver.get_disconnected_pvs() # doctest: +SKIP
        >>> len(pvs) # doctest: +SKIP
        70301
        >>> pprint(pvs[0]) # doctest: +SKIP
        {'commandThreadID': '0',
         'connectionLostAt': 'Jan/12/2022 11:07:08 +01:00',
         'hostName': 'pbi-bpm01-mtca-ioc01.tn.esss.lu.se',
         'instance': 'archiver-01',
         'internalState': 'Disconnected Searches: 9370',
         'lastKnownEvent': 'Jan/11/2022 15:44:56 +01:00',
         'noConnectionAsOfEpochSecs': '1641982028',
         'pvName': 'MEBT-010:PBI-BPM-007:BM-TR2-FirmwareVersion_RBV'}
        """
        return self._get("/getCurrentlyDisconnectedPVs")

    def get_event_rate_report(self, limit: int = 500) -> list:
        """Get PVs sorted by descending event rate.

        Javadoc: :archiver_mgmt_url:`reports/EventRateReport`

        :param limit:  Limit this report to this many PVs per appliance in the
            cluster, defaults to 500
        :type limit: int, optional
        :return:
        :rtype: list

        .. code-block:: python

            >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se')
            >>> from pprint import pprint
            >>> eventrate = archiver.get_event_rate_report(limit=1)
            >>> pprint(eventrate) # doctest: +SKIP
            [{'eventRate': '17.200530136821254',
              'pvName': 'LEBT-010:PBI-FC-001:RepellerTemp-R'},
             {'eventRate': '14.399567177689688',
              'pvName': 'TS2-010RFC:RFS-DIG-202:FreqSampMeas'}]

        """
        return self._get("/getEventRateReport", params={"limit": limit})

    def get_lost_connections(self, limit: int = 500) -> list[dict]:
        """Get number of times PVs lost/reestablished connections.

        Javadoc: :archiver_mgmt_url:`reports/LostConnectionsReport`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs sorted by number of times lost/reestablished connections
            to the IOC hosting the PV.
        :rtype: list[dict]

        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se') # doctest: +SKIP
        >>> from pprint import pprint  # doctest: +SKIP
        >>> pvs = archiver.get_lost_connections(limit=1) # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        [{'currentlyConnected': 'Yes',
          'instance': 'archiver-01',
          'lostConnections': '79',
          'pvName': 'TD-M:Ctrl-SCE-1:Seq-Sts'},
         {'currentlyConnected': 'Yes',
          'instance': 'archiver-02',
          'lostConnections': '19',
          'pvName': 'MBL-020RFC:RFS-VacPS-120:I'}]

        """
        return self._get("/getLostConnectionsReport", params={"limit": limit})

    def _get_matching_pvs_for_this_appliance(
        self, pv_name: str = "", regex: str = "", limit: int = 500
    ) -> list[str]:
        """Get matching PV's for this appliance.

        Javadoc: :archiver_mgmt_url:`GetMatchingPVsForAppliance`
        Note this call can return millions of PV names.

        :param pv_name: Name of PV that can contain a glob wildcard, defaults to ""
        :type pv_name: str, optional
        :param regex: Java regex wildcard, defaults to ""
        :type regex: str, optional
        :param limit: Maximum number of matched pv_names returned, defaults to 500
        :type limit: int, optional
        :return: matching pv_names for this appliance if one of ``pv_name`` or
            ``regex`` are specified. If both are specified, only the ``pv_name``
            will be applied. If neither is specified, and empty list will be returned
        :rtype: list[str]


        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se') # doctest: +SKIP
        >>> archiver.get_matching_pvs_for_this_appliance(
        ...    pv_name ="RFQ*", limit=3,
        ... ) # doctest: +SKIP
        ['RFQ-010:RFS-DIG-101:AI9-Lbl',
         'RFQ-010:RFS-DIG-102:CtrlIn-Q',
         'RFQ-010:RFS-DIG-102:CtrlIn-I']

        """
        return self._get(
            "/getMatchingPVsForThisAppliance",
            params={"pv": pv_name, "regex": regex, "limit": limit},
        )

    def get_meta_gets(self) -> list:
        """Get PVs that are currently in METAINFO_REQUESTED requested state.

        Javadocs: :archiver_mgmt_url:`reports/MetaGetsAction`

        :return: PVs that are currently in METAINFO_REQUESTED requested state in the
            archive workflow.
        :rtype: list

        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se') # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> pvs = archiver.get_meta_gets() # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        []

        """
        return self._get("/getMetaGets")

    def get_never_connected_pvs(self) -> list:
        """Get a list of PVs that have never connected.

        Javadoc: :archiver_mgmt_url:`reports/NeverConnectedPVsAction`

        :return: PVs that have never connected.
        :rtype: list

        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se') # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> pvs = archiver.get_never_connected_pvs() # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        []

        """
        return self._get("/getNeverConnectedPVs")

    def _get_paused_pvs(self, limit: int = 500) -> list:
        """Get PVs that are currently paused.

        Javadoc: :archiver_mgmt_url:`reports/PausedPVsReport`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs that are currently paused.
        :rtype: list

        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se') # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> pvs = archiver.get_paused_pvs(limit = 1) # doctest: +SKIP
        >>> len(pvs) # doctest: +SKIP
        466

        """
        return self._get("/getPausedPVsReport", params={"limit": limit})

    def get_pv_details(self, pv_name: str) -> list[dict[str, str]]:
        """Get detailed information about a PV.

        Javadoc: :archiver_mgmt_url:`reports/PVDetails`

        :param pv_name: The name of the PV
        :type pv_name: str
        :return: Detailed statistics for a PV
        :rtype: list[dict[str,str]]

        >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se') # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> details = archiver.get_pv_details('CWM-CWS03:WtrC-TT-020:Tmp')
        ...     # doctest: +SKIP
        >>> pprint(details) # doctest: +SKIP
        [{'name': 'PV Name', 'source': 'mgmt', 'value': 'CWM-CWS03:WtrC-TT-020:Tmp'},
         {'name': 'Instance archiving PV', 'source': 'mgmt', 'value': 'archiver-01'},
         {'name': 'Archival params creation time:',
          'source': 'mgmt',
          'value': 'Jan/17/2019 10:02:14 +01:00'},
         {'name': 'Archival params modification time:',
          'source': 'mgmt',
          'value': 'Feb/24/2021 14:09:42 +01:00'},
         {'name': 'Archiver DBR type (from typeinfo):',
          'source': 'mgmt',
          'value': 'DBR_SCALAR_DOUBLE'},
         {'name': 'Is this a scalar:', 'source': 'mgmt', 'value': 'Yes'},
         {'name': 'Number of elements:', 'source': 'mgmt', 'value': '1'},
         {'name': 'Precision:', 'source': 'mgmt', 'value': '0.0'},
         {'name': 'Units:', 'source': 'mgmt', 'value': ''},
         {'name': 'Is this PV paused:', 'source': 'mgmt', 'value': 'Yes'},
         {'name': 'Sampling method:', 'source': 'mgmt', 'value': 'MONITOR'},
         {'name': 'Sampling period:', 'source': 'mgmt', 'value': '0.07'},
         {'name': 'Are we using PVAccess?', 'source': 'mgmt', 'value': 'No'},
         {'name': 'Extra info - MDEL:', 'source': 'mgmt', 'value': '-1.0'},
         {'name': 'Extra info - ADEL:', 'source': 'mgmt', 'value': '0.0'},
         {'name': 'Extra info - SCAN:', 'source': 'mgmt', 'value': 'I/O Intr'},
         {'name': 'Extra info - RTYP:', 'source': 'mgmt', 'value': 'ai'},
         {'name': 'Archive Fields',
          'source': 'mgmt',
          'value': 'EGU,HIHI,HIGH,ADEL,LOW,LOLO,LOPR,HOPR'},
         {'name': 'Open channels', 'source': 'pv', 'value': '0'}]

        """
        return self._get("/getPVDetails", params={"pv": pv_name})

    def get_pv_status(self, pv_name) -> list[dict]:
        """Get the status of PV.

        Javadoc: :archiver_mgmt_url:`GetPVStatusAction`

        :param pv_name: name(s) of pv for which the status is to be
         determined. Can be a GLOB wildcards or multiple PVs as a comma
         separated list.
        :type pv: str
        :return: dictionaries with the status of matching PVs. If a pv is
         not being archived, you should get back a simple JSON object with
         a status string of "Not being archived.
        :rtype: list[dict]

        .. code-block:: python

            >>> archiver = ArchiveReader('archiver-01.tn.esss.lu.se')
            >>> from pprint import pprint
            >>> pprint(archiver.get_pv_status('CWM-CWS03:WtrC-TT-020:Tmp'))
            [{'appliance': 'archiver-01',
              'pvName': 'CWM-CWS03:WtrC-TT-020:Tmp',
              'pvNameOnly': 'CWM-CWS03:WtrC-TT-020:Tmp',
              'status': 'Paused'}]

        """
        return self._get("/getPVStatus", params={"pv": pv_name})

    def get_pv_type_info(self, pv_name: str) -> dict[str, str]:
        """Get the various archiving parameters for a PV.

        javadoc: :archiver_mgmt_url:`GetPVTypeInfo`

        :param pv_name: Name of the PV
        :type pv_name: str
        :return: information about the PV type
        :rtype: dict

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> from pprint import pprint
        >>> data = archiver.get_pv_type_info('CWM-CWS03:WtrC-TT-020:Tmp')
        >>> pprint(data)
        {'DBRType': 'DBR_SCALAR_DOUBLE',
         'applianceIdentity': 'archiver-01',
         'archiveFields': ['EGU',
                           'HIHI',
                           'HIGH',
                           'ADEL',
                           'LOW',
                           'LOLO',
                           'LOPR',
                           'HOPR'],
         'chunkKey': 'CWM/CWS03/WtrC/TT/020/Tmp:',
         'computedBytesPerEvent': '18',
         'computedEventRate': '0.6666667',
         'computedStorageRate': '12.516666',
         'creationTime': '2019-01-17T09:02:14.574Z',
            ...
         'elementCount': '1',
         'extraFields': {'ADEL': '0.0',
                         'MDEL': '-1.0',
                         'NAME': 'CWM-CWS03:WtrC-TT-020:Tmp',
                         'RTYP': 'ai',
                         'SCAN': 'I/O Intr'},
         'hasReducedDataSet': 'false',
         'hostName': 'cooling-rfq.tn.esss.lu.se',
         'lowerAlarmLimit': 'NaN',
         'lowerCtrlLimit': '0.0',
         'lowerDisplayLimit': '0.0',
         'lowerWarningLimit': 'NaN',
         'modificationTime': '2021-02-24T13:09:42.633Z',
         'paused': 'true',
         'policyName': 'default',
         'precision': '0.0',
         'pvName': 'CWM-CWS03:WtrC-TT-020:Tmp',
         'samplingMethod': 'MONITOR',
         'samplingPeriod': '0.07',
         'scalar': 'true',
         'units': '',
         'upperAlarmLimit': 'NaN',
         'upperCtrlLimit': '0.0',
         'upperDisplayLimit': '0.0',
         'upperWarningLimit': 'NaN',
         'useDBEProperties': 'false',
         'usePVAccess': 'false',
         'userSpecifiedEventRate': '0.0'}

        """
        return self._get("/getPVTypeInfo", params={"pv": pv_name})

    def get_pvs_by_dropped_events_buffer(self, limit: int = 500) -> list:
        """Get number of times events was lost for pvs because of buffer overflow.

        Javadoc: :archiver_mgmt_url:`reports/DroppedEventsBufferOverflowReport`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs sorted by number of events lost because of
            buffer overflows.
        :rtype: list

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se") # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> pvs = archiver.get_pvs_by_dropped_events_buffer(limit=1) # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        [{'eventsDropped': '27980856',
          'pvName': 'LEBT-010:PBI-FC-001:RepellerTemp-R'},
         {'eventsDropped': '220464117',
          'pvName': 'TS2-010RFC:RFS-DIG-201:FreqSampMeas'}]

        """
        return self._get("/getPVsByDroppedEventsBuffer", params={"limit": limit})

    def get_pvs_by_dropped_events_timestamp(self, limit: int = 500) -> list:
        """Get number of times events was lost for pvs because of incorrect timestamps.

        Javadoc: :archiver_mgmt_url:`reports/DroppedEventsTimestampReport`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs sorted by number of lost events due to incorrect
            timestamps.
        :rtype: list

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se") # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> pvs = archiver.get_pvs_by_dropped_events_timestamp(limit=1) # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        [{'eventsDropped': '104', 'pvName': 'MPSVac:Ctrl-IOC-01:DiagClkFlag'},
         {'eventsDropped': '11', 'pvName': 'MBL-020RFC:RFS-VacPS-220:I'}]

        """
        return self._get("/getPVsByDroppedEventsTimestamp", params={"limit": limit})

    def get_dropped_events_type_change(self, limit: int = 500) -> list:
        """Get number of lost events for pvs due to type changes.

        Javadoc: :archiver_mgmt_url:`reports/DroppedEventsTypeChangeReport`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs sorted by number lost events due to changes in
            type of the underlying PV. Does not include paused PVs.
        :rtype: list

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se") # doctest: +SKIP
        >>> from pprint import pprint # doctest: +SKIP
        >>> pvs = archiver.get_dropped_events_type_change(limit=1) # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        [{'eventsDropped': '30', 'pvName': 'RFQ-010:RFS-Mod-001:ForcedOff-RB'},
        {'eventsDropped': '2', 'pvName': 'KG-GTA:ODH-PLC-01:SW_Checksum'}]

        """
        return self._get("/getPVsByDroppedEventsTypeChange", params={"limit": limit})

    def get_recently_added_pvs(self, limit: int = 500) -> list[dict]:
        """Get PVs sorted by descending PVTypeInfo creation timestamp.

        Javadoc: :archiver_mgmt_url:`reports/RecentlyAddedPVs`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs sorted by descending PVTypeInfo creation timestamp.
        :rtype: list[dict]

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> from pprint import pprint
        >>> pvs = archiver.get_recently_added_pvs(limit=1) # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        [{'creationTime': 'Dec/08/2021 16:03:55 +01:00',
          'instance': 'archiver-01',
          'pvName': 'MEBT-010:Vac-VPI-50000:IonCurR'},
         {'creationTime': 'Jan/10/2022 18:14:06 +01:00',
          'instance': 'archiver-02',
          'pvName': 'TS2-010CRM:Cryo-LT-002:LvlR'}]
        """
        return self._get("/getRecentlyAddedPVs", params={"limit": limit})

    def get_recently_modified_pvs(self, limit: int = 500) -> list:
        """Get PVs sorted by descending PVTypeInfo modification timestamp.

        Javadoc: :archiver_mgmt_url:`reports/RecentlyChangedPVs`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs sorted by descending PVTypeInfo modification timestamp.
        :rtype: list

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> from pprint import pprint
        >>> pvs = archiver.get_recently_modified_pvs(limit=1) # doctest: +SKIP
        >>> pprint(pvs) # doctest: +SKIP
        [{'instance': 'archiver-01',
          'modificationTime': 'Feb/22/2019 10:21:43 +01:00',
          'pvName': 'LEBT-010:PBI-FC-001:AMC31-AOI11-ArrayData'},
        {'instance': 'archiver-02',
         'modificationTime': 'Nov/06/2019 14:51:36 +01:00',
         'pvName': 'TS2-010RFC:RFS01:Body-233:TE'}]
        """
        return self._get("/getRecentlyModifiedPVs", params={"limit": limit})

    def get_silent_pvs(self, limit: int = 500) -> list[dict]:
        """Get timestamp of the last event received of PVs.

        Javadoc: :archiver_mgmt_url:`reports/SilentPVReport`

        :param limit: Maximum number of PVs reported per appliance in the cluster,
            defaults to 500
        :type limit: int, optional
        :return: PVs sorted by the timestamp of the last event received (descending).
        :rtype: list[dict]

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> from pprint import pprint
        >>> pvs = archiver.get_silent_pvs(limit=1)
        >>> pprint(pvs)
        [{'instance': 'archiver-01',
          'lastKnownEvent': 'Never',
          'pvName': 'ISrc-010:TS-EVG-01:Mxc1-Frequency-RB'},
         {'instance': 'archiver-02',
          'lastKnownEvent': 'Never',
          'pvName': 'TS2-010CDL:Cryo-PID-EH60:FB_DEADB_1'}]

        """
        return self._get("/getSilentPVsReport", params={"limit": limit})

    def get_storage_rate_report(self, limit: int = 500) -> list[dict]:
        """Get PVs sorted by descending storage rate.

        Javadoc: :archiver_mgmt_url:`reports/StorageRateReport`

        :param limit: Maximum number of PVs reported per appliances, defaults to 500
        :type limit: int, optional
        :return: A report on PVs based on their storage rates.
        :rtype: list[dict]

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> from pprint import pprint
        >>> pvs = archiver.get_storage_rate_report(limit=1)
        >>> pprint(pvs) # doctest: +SKIP
        [{'pvName': 'LEBT-010:PBI-FC-001:TRC2-ArrayData-Comp',
          'storageRate_GBperYear': '2863.9619511724436',
          'storageRate_KBperHour': '342817.55330052465',
          'storageRate_MBperDay': '8034.786405481047'},
         {'pvName': 'TS2-010RFC:RFS-DIG-201:FreqSampMeas',
          'storageRate_GBperYear': '8.377073118442944',
          'storageRate_KBperHour': '1002.739477425163',
          'storageRate_MBperDay': '23.50170650215226'}]

        """
        return self._get("/getStorageRateReport", params={"limit": limit})

    def get_stores_for_pv(self, pv_name: str) -> list[str]:
        """Get the names and definitions of the data stores for this PV.

        Javadoc: :archiver_mgmt_url:`GetStoresForPV`
        Every store in a PV's typeinfo is expected to have a name - this is typically
        "name=STS" or something similar.

        :param pv_name: Name of the PV
        :type pv_name: str
        :return: Names of all the stores for a PV with their URI representations as a
            dictionary.
        :rtype: list[str]

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> archiver.get_stores_for_pv(pv_name="RFQ-010:Ctrl-IOC-01:ACCESS")
        ['STS', 'MTS', 'LTS']

        """
        return self._get("/getStoresForPV", params={"pv": pv_name})

    def _get_time_span(self) -> dict:
        """Get archiving time span.

        Javadoc: :archiver_mgmt_url:`reports/TimeSpanReport`

        :return: time span between first added PV to last known timestamp
         (if available), paused or not.
        :rtype: dict

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> timespan = archiver.get_time_span() # doctest: +SKIP
        >>> len(timespan) # doctest: +SKIP
        151138

        """
        return self._get("/getTimeSpanReport")

    def get_versions(self, appliance_id: str) -> dict[str, str]:
        """Get the versions of the various components for this appliance.

        Javadoc: :archiver_mgmt_url:`GetVersions`

        :param appliance_id: The identity of the appliance for which we are requesting
            information. This is the same string as the identity element in the
            appliances.xml that identifies this appliance.
        :type id: str
        :return: versions
        :rtype: dict[str,str]

        >>> archiver = ArchiveReader("archiver-01.tn.esss.lu.se")
        >>> from pprint import pprint
        >>> versions = archiver.get_versions("archiver-01")
        >>> pprint(versions)
        {'engine_version': 'Archiver Appliance Version '
                           '0.0.1_SNAPSHOT_15-November-2018T10-27-25',
        'etl_version': 'Archiver Appliance Version '
                       '0.0.1_SNAPSHOT_15-November-2018T10-27-25',
        'mgmt_version': 'Archiver Appliance Version '
                       '0.0.1_SNAPSHOT_15-November-2018T10-27-25',
        'retrieval_version': 'Archiver Appliance Version '
                       '0.0.1_SNAPSHOT_15-November-2018T10-27-25'}

        """
        return self._get("/getVersions", params={"id": appliance_id})
