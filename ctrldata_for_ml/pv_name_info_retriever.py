"""Module to parse and find information about PV names."""
from __future__ import annotations

import urllib
from string import Template

import regex  # type: ignore
import requests


class PVFields:
    """Finds and parses EPICS fields."""

    @staticmethod
    def enumerator(info: dict) -> dict:
        """Collect and structure enumerator fields in dictionary.

        :param info: dictionary with enumerator strings.
        :type info: dict
        :return: dictionary with enumerators collected
        :rtype: dict

        >>> info = {"ENUM_8":"enum 8",
        ... "CAPS_7": "other 7",
        ... "ENUM_0":"enum 0",
        ... "Uncaps_9":"uncaps 9",
        ... "ENUM_1":"enum 1",
        ... "NAN_n":"nan_n"}
        >>> PVFields.enumerator(info)
        {'Uncaps_9': 'uncaps 9',
         'NAN_n': 'nan_n',
         'ENUM': [(0, 'enum 0'), (1, 'enum 1'), (8, 'enum 8')],
         'CAPS': [(7, 'other 7')]}

        """
        pattern = regex.compile(r"^(?P<name>[A-Z]+)_(?P<index>[0-9]+)$")
        _info = info.copy()
        fields: dict = {}
        for name_index, obj in info.items():
            match = pattern.match(name_index)
            if match:
                _info.pop(name_index)
                name = match.groupdict()["name"]
                index = int(match.groupdict()["index"])
                tuples: list[tuple] = fields.setdefault(name, [])
                tuples.append((index, obj))
                _info[name] = sorted(tuples)
        return _info


class PVNameParser:
    """Finds and parses pv_names according to Naming Convention.

    >>> parser = PVNameParser()
    >>> from pprint import pprint
    >>> names = parser.parse("CWM-CWS03:WtrC-TT-010:Tmp-R.VAL")
    >>> pprint(names)
    {'device': 'CWM-CWS03:WtrC-TT-010',
     'device_type': 'WtrC-TT',
     'discipline': 'WtrC',
     'field': 'VAL',
     'idx': '010',
     'property': 'Tmp-R',
     'pv': 'CWM-CWS03:WtrC-TT-010:Tmp-R',
     'section': 'CWM',
     'subsection': 'CWM-CWS03'}
    """

    def _mnemonic_rules(self) -> dict:
        """Rules for mnemonics Sec, Sub, Dis, Dev, Idx, Prp and Fld."""
        rules = {
            "sec": r"(?P<section>[0-9a-zA-Z]{1,6})",  # Section
            "sub": r"[0-9a-zA-Z]{1,6}",  # Subsection
            "dis": r"(?P<discipline>[0-9a-zA-Z]{1,6})",  # Discipline
            "dev": r"[0-9a-zA-Z]{1,6}",  # Device type
            "idx": r"(?P<idx>[0-9a-zA-Z]{1,6})",  # instance index
            "prp": r"(?P<property>[-_0-9a-zA-Z]{1,20})",  # property
            "fld": r"(?P<field>[0-9a-zA-Z]{1,4})",  # field
        }
        return rules

    def _area_part_rule(self) -> str:
        """Rule for area (Sec-Sub or Sec)."""
        return r"(?:(?P<subsection>$sec-$sub)|$sec)"

    def _device_type_rule(self) -> str:
        """Rule for device type (Dis-Dev)."""
        return r"(?P<device_type>$dis-$dev)"

    def _device_rule(self) -> str:
        """Rule for device name (Sec-Sub:Dis-Dev-Idx or Sec-Sub:Dis-Dev)."""
        device_part_rule = rf"(?:{self._device_type_rule()}(?:-$idx)?)"
        return rf"(?P<device>{self._area_part_rule()}:{device_part_rule})"

    def _pv_rule(self) -> str:
        """Define the rule for pv name (Sec-Sub:Dis-Dev-Idx:Prt or Sec-Sub::Prt)."""
        normal_rule = rf"{self._device_rule()}:$prp"
        alternative_rule = rf"{self._area_part_rule()}::$prp"
        pv_name = rf"(?:{normal_rule})|(?:{alternative_rule})"
        return rf"(?P<pv>{pv_name})(?:.$fld)?"

    @property
    def pv_name_pattern(self) -> regex.Pattern:
        """Define pattern as a regular expression to match pv names against.

        :meta private:
        """
        template = Template(self._pv_rule())
        pattern_str = template.substitute(**self._mnemonic_rules())
        return regex.compile(pattern_str)

    def find(self, text: str) -> list[str] | None:
        """Find all matches of pv name in text.

        The text is scanned left-to-right and matched strings are returned in
        the order found.

        :param text: string to be searched and matched against.
        :type text: str
        :return: A list of string with all non-overlapping matches.
            None if no match.
        :rtype: list[str] | None

        >>> parser = PVNameParser()
        >>> parser.find("Both Sec-Sub:Dis-Dev-Idx:Prp, and Sys::Prp.HIGH!")
        ['Sec-Sub:Dis-Dev-Idx:Prp', 'Sys::Prp.HIGH']

        """
        matches = self.pv_name_pattern.finditer(text)
        if matches:
            return [m.group() for m in matches]
        else:
            return None

    def parse(self, pv_name: str) -> dict[str, str] | None:
        """Parse a PV name and creates a dictionary with name components.

        :param pv_name: the PV name
        :param type: str
        :return: namepart dictionary if the name matches the pattern, None
            otherwise.
        :rtype: Optional[dict[str, str]]

        >>> parser = PVNameParser()
        >>> from pprint import pprint
        >>> pv_name_components = parser.parse("CWM-CWS03:WtrC-TT-010:Tmp.VAL")
        >>> pprint(pv_name_components)
        {'device': 'CWM-CWS03:WtrC-TT-010',
         'device_type': 'WtrC-TT',
         'discipline': 'WtrC',
         'field': 'VAL',
         'idx': '010',
         'property': 'Tmp',
         'pv': 'CWM-CWS03:WtrC-TT-010:Tmp',
         'section': 'CWM',
         'subsection': 'CWM-CWS03'}

        """
        name_parts = self.pv_name_pattern.fullmatch(pv_name)
        if name_parts:
            return name_parts.groupdict()
        else:
            return None


class PVNameInfoRetriever:
    """Retrieve information about pv names from Naming service.

    :param hostname: hostname to naming service, defaults to "localhost"
    :type hostname: str, optional

    >>> name_retriever = PVNameInfoRetriever("naming.esss.lu.se")
    >>> from pprint import pprint
    >>> name_info = name_retriever("CWM-CWS03:WtrC-TT-010:Tmp.VAL")
    >>> pprint(name_info)
    [{'convention_name': 'CWM-CWS03:WtrC-TT-010:Tmp',
      'description': None,
      'mnemonic': 'Tmp',
      'name_type': 'pv',
      'status': None,
      'uuid': None},
     {'convention_name': 'CWM-CWS03:WtrC-TT-010',
      'description': 'RFQ - Exchanger regulation loop - TT10 - temperature '
                     'regulation and heater.',
      'mnemonic': '010',
      'name_type': 'device',
      'status': 'ACTIVE',
      'uuid': 'd516b1a6-6676-4654-860b-78fcffc62fd9'},
     {'convention_name': 'WtrC-TT',
      'description': 'Temperature transmitter',
      'mnemonic': 'TT',
      'name_type': 'device_type',
      'status': 'Approved',
      'uuid': 'c3b64265-6891-4738-9051-03f1944eb279'},
     {'convention_name': 'WtrC',
      'description': 'Water Cooling',
      'mnemonic': 'WtrC',
      'name_type': 'discipline',
      'status': 'Approved',
      'uuid': '989669d0-79b5-4b12-b6ec-717f458da2b4'},
     {'convention_name': 'CWM-CWS03',
      'description': 'Cooling Water System 03 (RFQ)',
      'mnemonic': 'CWS03',
      'name_type': 'subsection',
      'status': 'Approved',
      'uuid': 'e81b478f-a3a7-4e2a-b7a8-3aa73ba2c76e'},
     {'convention_name': 'CWM',
      'description': 'Cooling Water Medium System',
      'mnemonic': 'CWM',
      'name_type': 'section',
      'status': 'Approved',
      'uuid': '2010e7af-721d-4832-8877-b5c0550863ae'}]
    """

    def __init__(self, hostname: str = "localhost") -> None:
        """Construct.

        :param hostname: hostname, defaults to "localhost"
        :type hostname: str, optional
        """
        self.hostname = hostname
        self.session = requests.Session()
        self.parser = PVNameParser()
        self._rest_url = f"https://{self.hostname}/rest/"
        self._url_subpart = f"{self._rest_url}/parts/mnemonicPath/search/"
        self._url_part = f"{self._rest_url}/parts/mnemonic/search/"
        self._url_device = f"{self._rest_url}/deviceNames/search/"

    def health_check(self) -> int:
        """Check status of naming service.

        :return: HTTP response code
        :rtype: int

        >>> name_finder = PVNameInfoRetriever("naming.esss.lu.se")
        >>> name_finder.health_check()
        200

        """
        url = urllib.parse.urljoin(self._rest_url, "healthcheck")
        response = self.session.request("GET", url)
        response.raise_for_status()
        return response.status_code

    def _url(self, name_type: str) -> str:
        """Get the search url for a given name_type.

        :param name_type: section, subsection, discipline, device_type or
            device
        :type name_type: str

        :return: url to search convention name of type 'name_type'
        :rtype: str
        """
        if name_type == "device":
            return self._url_device
        elif (name_type == "section") or (name_type == "discipline"):
            return self._url_part
        elif (name_type == "subsection") or (name_type == "device_type"):
            return self._url_subpart
        else:
            raise ValueError("Invalid name_type: " + name_type)

    def _get(self, search_url: str, endpoint: str) -> dict:
        r"""Send a GET request to the given endpoint.

        :param endpoint: API endpoint (relative or absolute)
        :type endpoint: str
        :param search_url: API url
        :type search_url: str

        :returns: data dictionary
        :rtype: dict
        """
        url = urllib.parse.urljoin(search_url, endpoint.replace(":", r"\:").lstrip("/"))

        response = self.session.request(
            "GET", url, headers={"accept": "application/json"}
        )

        response.raise_for_status()
        return response.json()[0]

    @staticmethod
    def name_element(
        convention_name: str,
        name_type: str,
        mnemonic: str | None = None,
        description: str | None = None,
        status: str | None = None,
        uuid: str | None = None,
    ) -> dict[str, str | None]:
        """Create a dictionary with naming convention data.

        :param convention_name: naming convention name of pv, device,
            device_type, discipline, subsection or section
        :type convention_name: str
        :param name_type: pv, device, device_type, discipline, subsection
            or section
        :type name_type: str
        :param mnemonic: abbreviation, acronym or instance index
        :type mnemonic: str | None, optional
        :param description: Description
        :type description: str | None, optional
        :param status: Name status (approved, active, etc.)
        :type status: str | None, optional
        :param uuid: Unique identifier.
        :type uuid: str | None, optional
        :return: dictionary of parameters.
        :rtype: Dict[str, str | None]]
        """
        name_data = {
            "convention_name": convention_name,
            "mnemonic": mnemonic,
            "name_type": name_type,
            "description": description,
            "status": status,
            "uuid": uuid,
        }
        return name_data

    def _convention_name_data(self, convention_name: str, name_type: str):
        """Read data for a naming convention name.

        :param convention_name: Naming convention name for section (sec),
            subsection (sec-sub), discipline (dis),
            device_type (dis-dev) or device (sec-sub:dis-dev-idx)
        :type convention_name: str
        :param name_type: section, subsection, discipline, device_type or
            device.
        :type name_type: str
        :returns: dictionary with description, full name, status, mnemonic)
        :rtype: dict
        """
        data = self._get(self._url(name_type), convention_name)
        if name_type == "device":
            name_data = self.name_element(
                convention_name=data["name"],
                name_type=name_type,
                mnemonic=data["instanceIndex"],
                description=data["description"],
                status=data["status"],
                uuid=data["uuid"],
            )
        else:
            name_data = self.name_element(
                convention_name=data["mnemonicPath"],
                name_type=name_type,
                mnemonic=data["mnemonic"],
                description=data["name"],
                status=data["status"],
                uuid=data["uuid"],
            )
        return name_data

    def __call__(self, pv_name: str) -> list[dict]:
        """Retrieve information about pv names.

        :param pv_name: PV name
        :type pv_name: str
        :raises TypeError: If `pv_name` could not be parsed according to naming rules.
        :return: list of dictionaries with information about the
            name components of the pv name.
        :rtype: list([dict])
        """
        convention_names = self.parser.parse(pv_name)
        if convention_names is None:
            raise TypeError(f"{pv_name} could not be parsed.")

        # pv_names are not stored in naming service and need special treatment
        pv_name_data = self.name_element(
            convention_names["pv"],  # type: ignore
            "pv",
            mnemonic=convention_names["property"],  # type: ignore
        )
        result = [pv_name_data]
        keys = ["device", "device_type", "discipline", "subsection", "section"]
        for name_type in keys:
            convention_name = convention_names[name_type]  # type: ignore
            if convention_name:
                try:
                    data = self._convention_name_data(convention_name, name_type)
                    result.append(data)
                except TypeError:
                    pass
        return result
