"""Module to retrieve archived pv data."""
from __future__ import annotations

import json
import os
from argparse import ArgumentError

import h5py  # type: ignore
import matplotlib  # type: ignore
import matplotlib.pyplot as plt  # type: ignore
import pandas as pd  # type: ignore

from ctrldata_for_ml.archive_reader import ArchiveReader
from ctrldata_for_ml.pv_name_info_retriever import PVFields, PVNameInfoRetriever
from ctrldata_for_ml.time_machine import TimeMachine


class PVDataSet:
    """Class based on hdf5 to handle information about a set of pvs.

    :param path to hdf_file_name: path
    :type pv_name: str

    `Todo:`
        Replace with Nexus
        * https://confluence.esss.lu.se/display/SW/HDF5+and+NeXus+the+format
        * ESS-3179722
    """

    def __init__(
        self,
        path: str,
    ) -> None:
        """
        Construct.

        :param path: path to hdf5 file
        :type path: str
        """
        self.path = path

    def __repr__(self):
        """Represent."""
        return f"{self.__class__.__name__}({self.path})"

    @staticmethod
    def subgroup_key(key: str, pv_name: str = None) -> str:
        """Generate key to data store."""
        if pv_name:
            return f"{key}/{pv_name.replace(':' , '_').replace('-' , '_')}"
        return key

    def _data_frame(self, key: str) -> pd.DataFrame:
        """Get dataframe for key.

        :param key: name of the group
        :type key: str
        :return: data from store
        :rtype: pd.DataFrame
        """
        return pd.read_hdf(self.path, key=key)

    @property
    def exist(self) -> bool:
        """Check if data is available."""
        try:
            result = bool(self.file)
            return result
        except FileNotFoundError:
            return False

    @property
    def file(self):
        """Hdf5 File object."""
        return h5py.File(self.path)

    @property
    def archiving_info(self) -> pd.DataFrame:
        """General archiving info about process variables."""
        return self._data_frame("archiving_info")

    @property
    def categorical_fields(self) -> pd.DataFrame:
        """For categorical PVs, the enum mapping between values and labels."""
        return self._data_frame("categorical_fields")

    @property
    def convention_names(self) -> pd.DataFrame:
        """Convention names."""
        return self._data_frame("convention_names")

    def raw_data(self, pv_name: str) -> pd.DataFrame:
        """Raw data for a pv."""
        return self._data_frame(self.subgroup_key("raw_data", pv_name))

    @property
    def sampled_data(self) -> pd.DataFrame:
        """Resampled data set on a periodic time index."""
        return self._data_frame("sampled_data")

    @property
    def metadata(self) -> pd.DataFrame:
        """Get metadata fields for the pv set."""
        return self._data_frame("metadata")

    @property
    def archived_fields(self) -> pd.DataFrame:
        """Get dynamic metadata fields fields for the pv set."""
        return self._data_frame("archived_fields")

    @property
    def lost_connections(self) -> pd.DataFrame:
        """Lost connections with the archiver."""
        try:
            result = self._data_frame("lost_connections")
            return result
        except KeyError:
            return None

    def plot(
        self,
        raw: bool = False,
        averaged: bool = True,
        state_pv_name: str | None = None,
    ) -> matplotlib.axes.Axes:
        """Plot dataset.

        :param raw: if True raw pv data from data.val will be included,
         defaults to False
        :type raw: bool, optional
        :param averaged: if True time averaged data will be included,
         defaults to True
        :type averaged: bool, optional
        :param state_pv_name: pv holding state information, defaults to None
        :rtype state_pv_name: str | None, optional
        :return: list of axes to be plotted.
        :rtype: list[matplotlib.axes.Axes]

        """
        pv_names = list(self.archiving_info.index)
        plots = len(pv_names)
        _, axes = plt.subplots(
            plots, figsize=(16, 5 * plots), constrained_layout=True, sharex=True
        )
        plt.style.use("bmh")

        for i, name in enumerate(pv_names):
            axes[i] = self._plot_axes(name, axes[i], raw, averaged, state_pv_name)

        return axes

    def _plot_axes(
        self,
        pv_name: str,
        axes,
        raw: bool = False,
        averaged: bool = True,
        state_pv_name: str | None = None,
    ):
        """Plot data.

        :param pv_name: Name of pv to plot
        :type pv_name: str
        :param axes: Axes for the plot
        :type axes: matplotlib.axes.Axes
        :param raw: if True raw pv data from data.val will be included,
            defaults to False
        :type raw: bool, optional
        :param averaged: if True time averaged data will be included,
            defaults to True
        :type averaged: bool, optional
        :param state_pv_name: pv holding state information, defaults to None
        :rtype state_pv_name: str | None, optional
        :return: Axes to be plotted.
        :rtype: matplotlib.axes.Axes
        """
        sample: pd.DataFrame = self.sampled_data[pv_name]
        time: pd.DatetimeIndex = sample.index
        convention_names = self.convention_names.loc[pv_name, "description"]
        pv_description: str = convention_names.loc["pv"]
        try:
            device_description: str = convention_names.loc["device"]
            title = f"{pv_name} ({device_description})"
        except KeyError:
            title = pv_name

        axes.set_title(title)
        axes.set_ylabel(pv_description)
        axes.set_xlim(time[0], time[-1])
        axes.set_xlabel(time.name)

        try:
            enums = self.metadata.loc[pv_name, "ENUM"]
            if enums:
                ticks, labels = zip(*enums)
                axes.set_yticks(ticks, minor=False)
                axes.set_yticklabels(labels)
                ymin = min(ticks) - 1
                ymax = max(ticks) + 1
                axes.set_ylim(ymin, ymax)
        except KeyError:
            pass

        if state_pv_name:
            state_data: pd.DataFrame = self.raw_data(state_pv_name)
            state_enums = self.metadata.loc[state_pv_name, "ENUM"]
            ticks, labels = zip(*state_enums)
            keys = state_data["val"]
            alphas = 0.5 * (1 - keys / max(ticks))
            t_min = keys.index.to_series()
            t_max = t_min.shift(-1, fill_value=time[-1])
            for (left, right, alpha) in zip(t_min, t_max, alphas):
                axes.axvspan(
                    left,
                    right,
                    facecolor="grey",
                    alpha=alpha,
                )

        if self.lost_connections is not None:
            cnxlost = self.lost_connections
            cnxlost = cnxlost[cnxlost["name"] == pv_name]
            x_min = TimeMachine.trim(
                cnxlost["lost"], self.raw_data(pv_name).index.to_series()
            )
            x_max = cnxlost["regained"]
            for (left, right) in zip(x_min, x_max):
                axes.axvspan(
                    left,
                    right,
                    facecolor="yellow",
                    alpha=0.5,
                    label="lost connection with archiver",
                )

        # raw data
        if raw:
            val = self.raw_data(pv_name)["val"]
            axes.step(
                val.index,
                val,
                ".",
                where="post",
                label="raw data",
                color="blue",
                linewidth=0.5,
                alpha=0.3,
            )

        # Average:
        if averaged:
            average = sample["average"]
            axes.step(
                time,
                average,
                where="post",
                label="average",
                linewidth=1,
                color="c",
                alpha=1,
            )
            error = 2 * sample["variance"] ** (1 / 2)
            low = average - error
            high = average + error
            axes.fill_between(
                time, low, high, step="post", label="± 2 stddev", color="c", alpha=0.2
            )

        if self.metadata.loc[pv_name, "RTYP"] == "ai":
            zero = sample[sample["variance"] == 0]
            axes.step(
                zero.index,
                zero["average"],
                ".",
                where="post",
                label="vanishing variance",
                color="magenta",
                linewidth=0.3,
                alpha=0.3,
            )

        axes.legend()
        return axes


class PVRetriever:
    """Class to collect information about process variables.

    :param archiver_hostname: archiver hostname,
     defaults to "archiver-01.tn.esss.lu.se"
    :type archiver_hostname: str, optional
    :param timezone: local timezone, defaults to "Europe/Stockholm"
    :type timezone: str, optional
    :param begin: lower timelimit for batch, defaults to "1 hour"
    :type begin: str, optional
    :param end: Upper timelimit for batch, defaults to None
    :type end: str, optional
    :param sample_period: period length of bins, defaults to '1 minute'
    :type sample_period: str, optional
    :param naming_hostname: hostname of naming service,
     defaults to "naming.esss.lu.se"
    :type naming_hostname: str, optional
    :param path: Path to output directory for h5 files, defaults to './data'
    :param path: str, optional

    >>> pvr = PVRetriever(
    ...    archiver_hostname = "archiver-01.tn.esss.lu.se",
    ...    timezone = "Europe/Stockholm",
    ...    begin = "2021-10-30T14:00:00",
    ...    end ="2021-10-30T15:00:00",
    ...    sample_period="10 s",
    ...    naming_hostname = "naming.esss.lu.se")
    >>> pv = pvr.archived_data('CWM-CWS03:WtrC-TT-020:Temperature')
    >>> pv.sampled_data.head()
                CWM-CWS03:WtrC-TT-020:Temperature
                                 average  variance
    Time
    2021-10-30 14:00:00+02:00  28.792703  0.000064
    2021-10-30 14:00:10+02:00  28.793514  0.000058
    2021-10-30 14:00:20+02:00  28.798843  0.000088
    2021-10-30 14:00:30+02:00  28.801609  0.000068
    2021-10-30 14:00:40+02:00  28.800443  0.000059

    """

    _RELEVANT = [
        "appliance",
        "hostName",
        "connectionState",
        "paused",
        "creationTime",
        "modificationTime",
        "lastEvent",
        "DBRType",
        "samplingPeriod",
        "computedEventRate",
        "computedBytesPerEvent",
        "computedStorageRate",
        "hasReducedDataSet",
    ]

    def __init__(
        self,
        archiver_hostname: str = "archiver-01.tn.esss.lu.se",
        timezone: str = "Europe/Stockholm",
        begin: str = "1 hour",
        end: str = None,
        sample_period: str = "1 minute",
        naming_hostname: str = "naming.esss.lu.se",
        path: str = "./data",
    ):
        """Construct.

        :param archiver_hostname: archiver hostname,
            defaults to "archiver-01.tn.esss.lu.se"
        :type archiver_hostname: str, optional
        :param timezone: local timezone, defaults to "Europe/Stockholm"
        :type timezone: str, optional
        :param begin: lower timelimit for batch, defaults to "1 hour"
        :type begin: str, optional
        :param end: Upper timelimit for batch, defaults to None
        :type end: str, optional
        :param sample_period: period length of bins, defaults to '1 minute'
        :type sample_period: str, optional
        :param naming_hostname: hostname of naming service,
            defaults to "naming.esss.lu.se"
        :type naming_hostname: str, optional
        :param path: Path to output directory for h5 files, defaults to './data'
        :param path: str, optional
        """
        self.time_machine = TimeMachine(
            timezone=timezone, begin=begin, end=end, sample_period=sample_period
        )
        self.batch_span = next(self.time_machine)
        self.archiver = ArchiveReader(archiver_hostname)
        self.name_finder = PVNameInfoRetriever(naming_hostname)
        self.path = path

    def get_convention_names(
        self, pv_name: str, description: str = "", unit: str = ""
    ) -> pd.DataFrame:
        """Read out all name components for the pv from naming service.

        :param pv_name: name of the PV
        :type pv_name: str
        :param description: description of the pv. (DESC field), defaults to ''
        :type description: str, optional
        :param unit: Enginering unit of t the pv (EGU field), defaults to''
        :return: Table of convention names and information for pv, device,
            device type, discipline, subsection and section
        :rtype: pd.DataFrame

        >>> pvr = PVRetriever(naming_hostname = "naming.esss.lu.se")
        >>> names = pvr.get_convention_names(
        ...     "CWM-CWS03:WtrC-TT-020:Tmp",
        ...     "Temperature",
        ...     "degC",
        ... )
        >>> names.loc['CWM-CWS03:WtrC-TT-020:Tmp','description']
        name_type
        pv                                            Temperature (degC)
        device         RFQ - Body regulation loop - TT20 - temperatur...
        device_type                              Temperature transmitter
        discipline                                         Water Cooling
        subsection                         Cooling Water System 03 (RFQ)
        section                              Cooling Water Medium System
        Name: description, dtype: object

        """
        try:
            convention_names = self.name_finder(pv_name)
        except TypeError:
            pv_name_data = self.name_finder.name_element(pv_name, "pv")
            convention_names = [pv_name_data]
        data = pd.DataFrame.from_records(convention_names)
        data["name"] = pv_name
        data = data.astype(str)
        data = data.set_index(["name", "name_type"])

        if description and unit:
            data.loc[(pv_name, "pv"), "description"] = f"{description} ({unit})"
        elif unit:
            data.loc[(pv_name, "pv"), "description"] = f"({unit})"
        else:
            data.loc[(pv_name, "pv"), "description"] = description
        data = data.astype(str)

        return data

    def about(self, *args: str) -> pd.DataFrame | None:
        r"""Get overview information about process variables(s).

        :param args: Name of PV(s)
        :type args: str
        :return: dataframe and with pv_names as columns.
        :rtype: pd.DataFrame | None

        >>> pvr = PVRetriever(
        ...     timezone = "Europe/Stockholm",
        ...     archiver_hostname  = "archiver-01.tn.esss.lu.se"
        ... )
        >>> info = pvr.about('CWM-CWS03:WtrC-TT-020:Temperature')
        >>> info.T
                              CWM-CWS03:WtrC-TT-020:Temperature
        appliance                                   archiver-01
        hostName                                   172.16.60.42
        connectionState                                    True
        paused                                            False
        creationTime           2019-01-17 10:02:14.574000+01:00
        modificationTime       2021-02-24 14:09:47.136000+01:00
        lastEvent                       ...
        DBRType                               DBR_SCALAR_DOUBLE
        samplingPeriod                                     0.07
        computedEventRate                              0.666667
        computedBytesPerEvent                                18
        computedStorageRate                             12.5167
        hasReducedDataSet                                 False
        """
        status = self.pv_status(*args)
        if status is None:  # No archived pvs found.
            return None

        type_info = self.pv_type_info(*args)
        about = pd.concat([status, type_info], axis=1)
        about = about[self._RELEVANT]
        about = about.loc[:, ~about.columns.duplicated(keep="first")]
        return about

    def _get_details(self, *args: str) -> pd.DataFrame:
        r"""Get detailed info about a process variable.

        :param args: PV name(s)
        :type args: str
        :return: multiindexed dataframe and with pv_names as columns.
        :rtype: pd.DataFrame

        >>> pvr = PVRetriever(
        ...     timezone = "Europe/Stockholm",
        ...     archiver_hostname  = "archiver-01.tn.esss.lu.se"
        ... )
        >>> details = pvr._get_details('CWM-CWS03:WtrC-TT-020:Temperature')
        >>> details.loc["mgmt"].head()
                                           CWM-CWS03:WtrC-TT-020:Temperature
        property
        Instance archiving PV                                    archiver-01
        Archival params creation time:             2019-01-17 10:02:14+01:00
        Archival params modification time:         2021-02-24 14:09:47+01:00
        Archiver DBR type (from typeinfo):                 DBR_SCALAR_DOUBLE
        Is this a scalar:                                                Yes

        """
        _drop = [
            ("mgmt", "PV Name"),
            ("pv", "Channel Name"),
            ("etl", "Name (from ETL)"),
        ]
        frames = []
        for name in args:
            json_data_raw = self.archiver.get_pv_details(name)
            json_data = self.decode(json_data_raw)
            frames.append(
                pd.DataFrame.from_records(json_data, index=("source", "name"))
            )
        result = pd.concat(frames, axis=1)
        result.index.set_names(["source", "property"], inplace=True)
        result.columns = list(result.loc[_drop[0]])
        result.drop(_drop, axis=0, inplace=True)

        return result

    def get_pv_details(self, *args: str) -> pd.DataFrame:
        r"""Get detailed info about process variables.

        :param args: PV name(s)
        :type args: str
        :return: pv details
        :rtype: pd.DataFrame

        >>> pvr = PVRetriever(
        ...     timezone = "Europe/Stockholm",
        ...     archiver_hostname  = "archiver-01.tn.esss.lu.se"
        ... )
        >>> pv_details = pvr.get_pv_details('CWM-CWS03:WtrC-TT-020:Temperature')
        >>> pv_details.head()
                                            CWM-CWS03:WtrC-TT-020:Temperature
        property
        Host name                                                172.16.60.42
        Controlling PV
        Is engine currently archiving this?                               yes
        Archiver DBR type (initial)                         DBR_SCALAR_DOUBLE
        Archiver DBR type (from CA)                         DBR_SCALAR_DOUBLE
        """
        return self._get_details(*args).loc["pv"]

    def management_details(self, *args: str) -> pd.DataFrame:
        r"""Get management info for process variables.

        :param args: PV name(s)
        :type args: str
        :return: pv details
        :rtype: pd.DataFrame

        >>> pvr = PVRetriever(
        ...     timezone = "Europe/Stockholm",
        ...     archiver_hostname  = "archiver-01.tn.esss.lu.se"
        ... )
        >>> details = pvr.management_details('CWM-CWS03:WtrC-TT-020:Temperature')
        >>> details.head()
                                           CWM-CWS03:WtrC-TT-020:Temperature
        property
        Instance archiving PV                                    archiver-01
        Archival params creation time:             2019-01-17 10:02:14+01:00
        Archival params modification time:         2021-02-24 14:09:47+01:00
        Archiver DBR type (from typeinfo):                 DBR_SCALAR_DOUBLE
        Is this a scalar:                                                Yes
        """
        return self._get_details(*args).loc["mgmt"]

    def event_details(self, *args: str) -> pd.DataFrame:
        r"""Get event stream info for process variables.

        :param args: PV name(s)
        :type args: str
        :return: pv details
        :rtype: pd.DataFrame

        >>> pvr = PVRetriever(
        ...     timezone = "Europe/Stockholm",
        ...     archiver_hostname  = "archiver-01.tn.esss.lu.se"
        ... )
        >>> details = pvr.event_details('CWM-CWS03:WtrC-TT-020:Temperature')
        >>> details.head()
                                              CWM-CWS03:WtrC-TT-020:Temperature
        property
        ETL 0 partition granularity of source                    PARTITION_HOUR
        ETL 0 partition granularity of dest                       PARTITION_DAY
        ETL 0 last completed                            ...
        ETL 0 last job took (ms)                        ...
        ETL 0 next job runs at                          ...
        """
        return self._get_details(*args).loc["etl"]

    def pv_type_info(self, *args: str) -> pd.DataFrame:
        r"""Get pv type informaton from archiver.

        :param args: PV name(s)
        :type args: str
        :return: info about the PV(s)
        :rtype: pd.DataFrame

        >>> pvr = PVRetriever(
        ...     timezone = "Europe/Stockholm",
        ...     archiver_hostname  = "archiver-01.tn.esss.lu.se"
        ... )
        >>> pv_info = pvr.pv_type_info('CWM-CWS03:WtrC-TT-020:Temperature')
        >>> pv_info.T.head()
                        CWM-CWS03:WtrC-TT-020:Temperature
        hostName                             172.16.60.42
        paused                                      False
        creationTime     2019-01-17 10:02:14.574000+01:00
        lowerAlarmLimit                               NaN
        precision                                       0
        """
        dicts = []
        for name in args:
            raw_json_data = self.archiver.get_pv_type_info(name)
            json_data: dict = self.decode(raw_json_data)  # type: ignore
            dicts.append(json_data)
        frame = pd.DataFrame.from_records(dicts, index="pvName")
        frame.index.name = None
        return frame

    def pv_status(self, *args: str) -> pd.DataFrame:
        r"""Return archiving status of process variables.

        :param args: Name of the PV or PVs
        :type args: str
        :return: Status of PVs that are archived,
            None if no PVs were archived
        :rtype: pd.DataFrame

        >>> pvr = PVRetriever(
        ...     timezone = "Europe/Stockholm",
        ...     archiver_hostname  = "archiver-01.tn.esss.lu.se"
        ... )
        >>> pv_status = pvr.pv_status(
        ...    'not:archived:fake-pv','CWM-CWS03:WtrC-TT-020:Temperature'
        ... )
        >>> pv_status.T.head()
                                CWM-CWS03:WtrC-TT-020:Temperature
        lastRotateLogs                                        NaT
        appliance                                     archiver-01
        connectionState                                      True
        lastEvent                               ...
        samplingPeriod                                       0.07
        """
        pv_name = ",".join(args)
        status = self.archiver.get_pv_status(pv_name)
        status = self.decode(status)  # type: ignore
        result = pd.DataFrame.from_records(
            data=status,
            index="pvNameOnly",
            exclude=["pvName", "status"],
        )
        result.dropna(axis=0, how="all", inplace=True)
        if result.empty:
            return None
        result.index.name = None
        return result

    def decode(self, obj) -> dict | list | str | int | float | bool:
        """Decode dict of strings, list of strings or string to types.

        :param obj: either a string to be decoded or a list or dict containing
            strings to be decoded
        :type obj: [type]
        :return: decoded object or container with decoded objects.
        :rtype: dict | list | str | int | float | bool

        >>> pvr = PVRetriever(timezone="Europe/Stockholm")
        >>> obj = [
        ...     '{ "num":"1", "bool":"true", "time":"2020-01-01", "nan":"NaN"}',
        ...     '{ "num":"1.1", "bool":"false", "time":"Never", "nan":"None"}'
        ... ]
        >>> print(pvr.decode(obj))
        [{'num': 1,
          'bool': True,
          'time': Timestamp('2020-01-01 00:00:00+0100', tz='Europe/Stockholm'),
          'nan': nan},
         {'num': 1.1, 'bool': False, 'time': NaT, 'nan': 'None'}]
        """
        if not obj:
            return obj

        if isinstance(obj, str):
            try:
                value: dict | list | str | int | float | bool = json.loads(obj)
                return self.decode(value)
            except json.JSONDecodeError:
                return self.time_machine.parse(obj)
        elif isinstance(obj, dict):
            return {k: self.decode(s) for k, s in obj.items()}
        elif isinstance(obj, list):
            return [self.decode(s) for s in obj]
        else:
            return obj

    def _single_pv_data(
        self, pv_name: str
    ) -> tuple[pd.DataFrame, pd.DataFrame | None, dict | None, pd.DataFrame | None]:
        """Get data, fields metadata and cnxlost for one pv from the archiver.

        :param pv_name: Name of the PV
        :type pv_name: str
        :return: data, fields, metadata, cnxlost
        :rtype: tuple[pd.DataFrame | None, pd.DataFrame | None, dict,
         pd.DataFrame | None]

        >>> pvr = PVRetriever(
        ...    archiver_hostname = "archiver-01.tn.esss.lu.se",
        ...    timezone = "Europe/Stockholm",
        ...    begin = "2021-11-15T21:00:00",
        ...    end = "2021-11-17T21:00:00",
        ...    sample_period="10 s",
        ...    naming_hostname = "naming.esss.lu.se")
        >>> (data,fields,metadata, cnxlost) = pvr._single_pv_data(
        ...     "RFQ-010:RFS-FIM-101:RP1-Ilck-RB"
        ... )
        >>> data.head()
                                             val  severity  status
        Time
        2021-11-15 19:35:55.292873251+01:00    1         0       0
        2021-11-15 21:12:32.871297828+01:00    0         0       0
        2021-11-15 21:12:32.951396120+01:00    1         0       0
        2021-11-15 21:12:37.158785121+01:00    0         0       0
        2021-11-15 21:12:37.304970103+01:00    1         0       0
        >>> cnxlost[["lost","archiver_startup"]]
                                                                 lost  archiver_startup
        Time
        2021-11-16 10:19:41.591219031+01:00 1970-01-01 01:00:00+01:00              True
        >>> fields
                                            DESC
        Time
        2021-11-16 17:04:33.121984854+01:00
        2021-11-17 17:12:09.013766155+01:00

        >>> pd.Series(metadata)
        SCAN                             I/O Intr
        NAME      RFQ-010:RFS-FIM-101:RP1-Ilck-RB
        RTYP                                   bi
        DESC
        name      RFQ-010:RFS-FIM-101:RP1-Ilck-RB
        DRVH                                    0
        ENUM_1                                 OK
        HIGH                                    0
        HIHI                                    0
        ENUM_0                                NOK
        DRVL                                    0
        PREC                                    0
        LOLO                                    0
        LOPR                                    0
        HOPR                                    0
        LOW                                     0
        NELM                                    1
        dtype: object
        """
        [json_data] = self.archiver.get_data(
            pv_name,
            begin=self.batch_span.begin.isoformat(),
            end=self.batch_span.end.isoformat(),
            fetchLatestMetadata="true",
        )
        json_data = self.decode(json_data)  # type: ignore
        json_metadata: dict = json_data["meta"]
        data_w_fields: list[dict]
        data_w_fields = json_data["data"]  # type: ignore
        data, cnxlost, fields = self._separate_fields(data_w_fields)

        metadata = {}
        try:
            extra = self.pv_type_info(pv_name)["extraFields"].loc[pv_name]
            metadata.update(extra)
        except ArgumentError:
            pass
        if fields is not None:
            metadata.update(**fields.iloc[-1])
        if json_metadata:
            metadata.update(json_metadata)

        return data, fields, metadata, cnxlost

    def _separate_fields(
        self,
        json_data: list[dict[str, int | float | dict[str, str | int | float | bool]]],
    ) -> tuple[pd.DataFrame, pd.DataFrame | None, pd.DataFrame | None]:
        """Separates out fields and connection lost from archived data.

        :param json_data: data dictionary from archiver
        :type json_data: list[dict]
        :return: data, cnxlost and remaining fields (metadata)
        :rtype: tuple[pd.DataFrame, pd.DataFrame|None, pd.DataFrame|None]

        >>> pv_reader = PVRetriever(
        ...    archiver_hostname = "archiver-01.tn.esss.lu.se",
        ...    timezone = "Europe/Stockholm")
        >>> json_data = [
        ...   {'secs': 1637008562,
        ...    'val': 1,
        ...    'nanos': 477078628,
        ...    'severity': 0,
        ...    'status': 0},
        ...   {'secs': 1637054381,
        ...    'val': 1,
        ...    'nanos': 591219031,
        ...    'severity': 0,
        ...    'status': 0,
        ...    'fields': {'cnxlostepsecs': 0,
        ...    'startup': True,
        ...    'cnxregainedepsecs': 1637054381}}]
        >>> data, cnxlost, fields = pv_reader._separate_fields(json_data)
        >>> data
                                             val  severity  status
        Time
        2021-11-15 21:36:02.477078628+01:00    1         0       0
        2021-11-16 10:19:41.591219031+01:00    1         0       0

        >>> cnxlost[["lost","archiver_startup"]]
                                                                lost  archiver_startup
        Time
        2021-11-16 10:19:41.591219031+01:00 1970-01-01 01:00:00+01:00             True
        """
        # Convert json dict to pandas data frame
        data = pd.DataFrame.from_records(json_data)
        data = self.time_machine.time_frame(data, seconds="secs", nanoseconds="nanos")
        try:
            fields_series: pd.Series = data.pop("fields").dropna()
        except KeyError:
            return data, None, None

        # convert series of dictionaries to a data frame:
        fields = pd.DataFrame.from_records(fields_series, index=fields_series.index)

        # extract information about lost connections with archiver
        cnxlost_series = []

        try:
            regained_secs: pd.Series = fields.pop("cnxregainedepsecs").dropna()
            regained = self.time_machine.convert_columns(
                regained_secs.to_frame(), regained_secs.name, None
            )
            regained.name = "regained"
            cnxlost_series.append(regained)
        except KeyError:
            pass

        try:
            lost_secs: pd.Series = fields.pop("cnxlostepsecs").dropna()
            lost = self.time_machine.convert_columns(
                lost_secs.to_frame(), lost_secs.name, None
            )
            lost.name = "lost"
            cnxlost_series.append(lost)
        except KeyError:
            pass

        try:
            startup: pd.Series = fields.pop("startup").dropna()
            startup.name = "archiver_startup"
            cnxlost_series.append(startup)
        except KeyError:
            pass

        if cnxlost_series:
            cnxlost = pd.concat(
                cnxlost_series,
                axis=1,
            )
        else:
            cnxlost = None

        if fields.empty:
            fields = None
        else:
            fields.dropna(axis=0, how="all", inplace=True)

        return data, cnxlost, fields

    def update_batch_span(self) -> None:
        """Update to the next batch_span."""
        self.batch_span = next(self.time_machine)

    @property
    def data_set(self) -> PVDataSet:
        """Instance of PVDataSet."""
        begin = self.batch_span.begin
        timestamp = self.time_machine.short_isoformat(begin)
        period = self.batch_span.end - begin
        period_stamp = self.time_machine.short_isoformat(period)
        path = f"{self.path}/{period_stamp}"
        filename = f"{path}/{timestamp}.h5"
        if not os.path.exists(path):
            os.makedirs(path)
        return PVDataSet(filename)

    def archived_data(self, *args: str) -> PVDataSet:
        r"""Create sets of data from the archiver and naming service.

        :param args: The name of the pvs
        :type args: str
        :return: datafile
        :rtype: h5py._hl.files.File
        """
        pv_data_set = self.data_set
        filename = pv_data_set.path
        info = self.about(*args)
        if info is None:
            raise ValueError("No data.")

        info.to_hdf(filename, "archiving_info")
        pv_names = list(info.index)

        all_sampled_data: dict[str, pd.DataFrame] = {}
        all_fields: list[pd.DataFrame] = []
        all_metadata: list[dict] = []
        all_cnxlost: list[pd.dataFrame] = []
        all_names: list[pd.DataFrame] = []

        for name in pv_names:
            (data, fields, metadata, cnxlost) = self._single_pv_data(name)
            if data is not None:
                data.to_hdf(filename, pv_data_set.subgroup_key("raw_data", name))
                all_sampled_data[name] = self.batch_span.resample(data["val"])
            if fields is not None:
                fields["name"] = name
                all_fields.append(fields)
            if metadata:
                metadata = PVFields.enumerator(metadata)
                metadata.setdefault("ENUM", [])  # type: ignore
                all_metadata.append(metadata)
                description = str(metadata.setdefault("DESC", ""))
                unit = str(metadata.setdefault("EGU", ""))
                convention_names = self.get_convention_names(name, description, unit)
                all_names.append(convention_names)
            if cnxlost is not None:
                cnxlost["name"] = name
                all_cnxlost.append(cnxlost)

        if all_sampled_data:
            resample_data = pd.concat(
                all_sampled_data.values(), axis=1, keys=all_sampled_data.keys()
            )
            resample_data.to_hdf(filename, "sampled_data")
        if all_fields:
            archived_fields = pd.concat(all_fields, axis=0)
            archived_fields.to_hdf(filename, "archived_fields")
        if all_metadata:
            last_metadata = pd.DataFrame.from_records(all_metadata, index="name")
            last_metadata.to_hdf(filename, "metadata")
        if all_cnxlost:
            connection_lost: pd.DataFrame = pd.concat(all_cnxlost, axis=0).sort_index()
            connection_lost.to_hdf(filename, "lost_connections")
        if all_names:
            convention_names = pd.concat(all_names, axis=0)
            convention_names.to_hdf(filename, "convention_names")

        return pv_data_set
