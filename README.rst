Control System Data for Machine learning
========================================

Python package to extract data from ess controls data sources and prepare it for
machine learning studies. The package is developed for the European Spallation
Source, but might be useful at similar facilities.


Required
--------

Python: ~3.7


Installation
------------

Pip Command:

.. code-block:: console

    $ pip install py-epicsarchiver -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple


Quick Start
-----------

.. code-block:: Python

    >>> from ctrldata_for_ml import PVRetriever
    >>> pvr = PVRetriever(
    ...    archiver_hostname = "archiver-01.tn.esss.lu.se",
    ...    timezone = "Europe/Stockholm",
    ...    begin = "2021-10-30T14:00:00",
    ...    end ="1 hour",
    ...    sample_period="10 s",
    ...    naming_hostname = "naming.esss.lu.se")
    >>> dataset = pvr.archived_data('CWM-CWS03:WtrC-TT-020:Temperature')
    >>> dataset.sampled_data.head()
                 CWM-CWS03:WtrC-TT-020:Temperature
                                 average  variance
    Time
    2021-10-30 14:00:00+02:00  28.792703  0.000064
    2021-10-30 14:00:10+02:00  28.793514  0.000058
    2021-10-30 14:00:20+02:00  28.798843  0.000088
    2021-10-30 14:00:30+02:00  28.801609  0.000068
    2021-10-30 14:00:40+02:00  28.800443  0.000059


Documentation pages:
--------------------

http://icshwi.pages.esss.lu.se/ai/control-system-data-for-machine-learning/index.html


Contribute:
-----------

.. _Poetry: https://python-poetry.org/docs/master/#installing-with-the-official-installer/

1. Install and configure `Poetry`_

2. Check out the code:

.. code-block:: console

    git clone git@gitlab.esss.lu.se:icshwi/ai/control-system-data-for-machine-learning.git

3. Navigate do the project folder and install the environment:

..  code-block:: console

    $ cd ctrldata-for-ml
    $ poetry install


License
-------

The project is licensed under the MIT license.
