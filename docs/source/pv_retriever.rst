pv_retriever
============

About
-----
Convenient module for data retrieval.

.. todo::

   * Add methods to output data on Nexus format.
   * Add data from alarm handler and ccdb.

API
---

The ``PVDataSet`` class
^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: pv_retriever.PVDataSet
   :members:
   :undoc-members:
   :show-inheritance:

The ``PVRetriever`` class
^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: pv_retriever.PVRetriever
   :members:
   :undoc-members:
   :show-inheritance:
