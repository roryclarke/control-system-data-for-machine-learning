.. ctrldata documentation master file, created by
   sphinx-quickstart on Wed Dec  1 02:00:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ctrldata-for-ml's documentation!
===========================================

.. toctree::
   :maxdepth: 4
   :caption: Overview

   overview

.. toctree::
   :maxdepth: 4
   :caption: Cookbook

   ..include ../../demo.ipynb

.. toctree::
   :maxdepth: 4
   :caption: Modules

   archive_reader
   pv_retriever
   pv_name_info_retriever
   time_machine


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
