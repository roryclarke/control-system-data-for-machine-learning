time_machine
============

About
-----
Time_machine is a convenient module based on Pandas to handle
time information for data retrieval, batching and re-sampling of control system data.
It is based on the following definitions and principles:

    #. *Time* is the number of elapsed nanoseconds past epoch
       1970-01-01:T00:00:00+00:00.
    #. *Timestamp* is the human-readable identity of a point in time
       when an event occurred.
    #. Timestamps generated by :class:`TimeMachine` are *timezone aware*
       :class:`pandas.Timestamp` objects.
    #. :class:`TimeMachine` interprets None as current time, integers as
       the number of nanoseconds since epoch, strings without timezone information as local time and strings without date information as time of the current day.
    #. *Time interval* is a semi-open interval in time including the beginning of the
       interval.
    #. Time intervals are timestamped *at the beginning* of intervals.
    #. Values of process variables (PV) are valid on the time intervals between
       acquisitions. Re-sampling to periodic time intervals is therefore based on *time-
       average*. Re-sampling also includes calculation of the variance to quantify
       noise and detect misssing data (vanishing variance).
    #. *Period* is the number of nanoseconds of time intervals.
    #. *Timestamps are precise but not accurate.* Precision is preserved to ensure
       accurate periods and one-to-one mapping between time and timestamps.


Examples
--------

Handling batches of PV data
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create a batch span of 1 hour with 1 min sample period:

.. code-block:: python

    >>> from ctrldata_for_ml import TimeMachine
    >>> time_machine = TimeMachine(
    ...    timezone = "Europe/Stockholm",
    ...    begin = "2021-10-10T07:00:00",
    ...    end = "1 hour",
    ...    sample_period = "5 min")
    >>> batch_span = next(time_machine)
    >>> batch_span
    BatchTimeSpan(2021-10-10 07:00:00+02:00, 2021-10-10 08:00:00+02:00,
    0 days 00:05:00)

Simulate random control system data with roughly 10 sec between acquisitions:

.. code-block:: python

    >>> data = batch_span.fake_data(average_period = "10 sec")
    >>> data.head()
             secs      nanos       val
    0  1633841999  605412033 -1.412368
    1  1633842032  553025244 -0.912730
    2  1633842039  112366044  0.654094
    3  1633842043  908690378  0.949929
    4  1633842045  580737773 -0.174250

Convert to a dataframe with :class:`pandas.DatetimeIndex` as index:

.. code-block:: python

    >>> data = time_machine.time_frame(dat)
    >>> data.head()
                                              val
    Time
    2021-10-10 06:59:59.605412033+02:00 -1.412368
    2021-10-10 07:00:32.553025244+02:00 -0.912730
    2021-10-10 07:00:39.112366044+02:00  0.654094
    2021-10-10 07:00:43.908690378+02:00  0.949929
    2021-10-10 07:00:45.580737773+02:00 -0.174250

resample this data:

.. code-block:: python

    >>> data = batch_span.resample(data["val"])
    >>> data.head()
                                average  variance
    Time
    2021-10-10 07:00:00+02:00 -0.222535  0.974695
    2021-10-10 07:05:00+02:00  0.135244  1.167973
    2021-10-10 07:10:00+02:00 -0.070052  0.716942
    2021-10-10 07:15:00+02:00 -0.401814  0.991538
    2021-10-10 07:20:00+02:00  0.003588  1.044658


Continue with the next batch:

.. code-block:: python

    >>> next_batch = next(time_machine)
    >>> next_batch
    BatchTimeSpan(2021-10-10 08:00:00+02:00, 2021-10-10 09:00:00+02:00, 0 days 00:05:00)


Motivation
----------

Preserve precision
^^^^^^^^^^^^^^^^^^
The C11 standard (ISO/IEC 9899:2011) class timespec, which EPICS uses, is a struct of seconds past epoch and nanoseconds within and has not been implemented in python.
The pythonic way used by e.g. :meth:`time.time_ns`, :meth:`numpy.Datetime64` and
:class:`pandas.DatetimeIndex` is nanoseconds past epoch of type integer, which
has unlimited precision and makes calculations easier. It is however
sensitive since as soon as floats enters the calculation the nanosecond precision drops
to microsecond precision. Note that the built in python module :mod:`datetime` is based
on floats and that :class:`pandas.Timestamp` extend both :meth:`numpy.Datetime64` and
:class:`datetime.datetime`.

Convert seconds and nanoseconds
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Handling time and time formats should be straightforward but lack of consensus on how
to interpret time information without zone information causes some confusion.

Intuitively naive time should be interpreted as local time (here and now), which is
related to the utc epoch (there and then).

The built-in python module :py:mod:`datetime` follows this principle:

.. code-block:: python

    >>> from datetime import datetime
    >>> datetime.fromtimestamp(0).isoformat() != '1970-01-01T00:00:00'
    True

Thus, printing the time at t = 0 yields the time here when the UTC epoch
occurred there.

:class:`numpy.datetime64` does not hold timezone information, nor day-light saving
information and is therefore always in UTC. :class:`pandas.Timestamp`, which extends
both these classes handles timezone information but naive time is treated as UTC:

.. code-block:: python

    >>> import pandas
    >>> pandas.Timestamp(0).isoformat() == '1970-01-01T00:00:00'
    True

Therefore, objects of :class:`pandas.Timestamp` needs to be localized to UTC and
thereafter converted to local timezone. This is handled by
:meth:`TimeMachine.time_frame` and
:meth:`TimeMachine.timestamp` methods:

.. code-block:: python

    >>> TimeMachine("Europe/Stockholm").timestamp(0).isoformat()
    '1970-01-01T01:00:00+01:00'

Data retrieval:
^^^^^^^^^^^^^^^
Isoformat time strings with zone informaition are required to retrieve data from e.g.
the EPICS Archiver Appliance. (Time zone information does however not need to be
converted to UTC, this seems to be a general misunderstanding. The
`dateTime
<http://joda-time.sourceforge.net/apidocs/org/joda/time/format/ISODateTimeFormat.html#dateTime()>`_
formatter used by Archiver Appliance parses timestamps of other timezones than UTC.).
Humans should neither have to type in timezone information to retrieve data, nor
should they have to use isostandard formats. This is solved by
:meth:`TimeMachine.timestamp`, which interprets input and localizes and converts
:class:`pandas.Timestamp` to the local timezone:

.. code-block:: python

    >>> time_machine.timestamp('Jun/01/2021 17:07:02').isoformat()
    '2021-06-01T17:07:02+02:00'

The :class:`BatchTimeSpan` class makes data retrieval straightforward:

.. code-block:: python

    >>> batch_span.begin.isoformat()
    '2021-10-10T07:00:00+02:00'
    >>> batch_span.end.isoformat()
    '2021-10-10T08:00:00+02:00'

Reduce data and improve data quality
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Averaging over a sample period and providing the variance
will speed up the training process for machine learning applications.
This will also reduce data
volumes to be stored and transferred over the network.

API
---

The ``TimeMachine`` class
^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: time_machine.TimeMachine
   :members:
   :undoc-members:
   :show-inheritance:

The ``BatchTimeSpan`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: time_machine.BatchTimeSpan
   :members:
   :undoc-members:
   :show-inheritance:
