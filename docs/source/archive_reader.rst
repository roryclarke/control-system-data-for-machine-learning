archive_reader
==============

About
-----
Interface to archiver appliance.

   * Only GET methods implemented.
   * Outputs raw json data, no conversion is made.

For parsing to other formats, see e.g. :py:mod:`pv_retriever`


API
---

The ``ArchiveReader`` class
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: archive_reader.ArchiveReader
   :members:
   :undoc-members:
   :show-inheritance:
