# pylint: skip-file
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
from __future__ import annotations

import os
import sys

sys.path.insert(0, os.path.abspath("../../ctrldata_for_ml/"))
sys.path.insert(0, os.path.abspath("../.."))


# -- Project information -----------------------------------------------------

project = "ctrldata-for-ml"
copyright = "2021, European Spallation Source ERIC"
author = "Karin Rathsman"

# The full version, including alpha/beta/rc tags

try:
    #    CI_COMMIT_REF_NAME is defined by GitLab Runner
    release = os.environ["CI_COMMIT_REF_NAME"]
except KeyError:
    # dev mode
    release = os.popen("git describe").read().strip()

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_rtd_theme",
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.extlinks",
    "sphinx.ext.autosummary",
    "sphinx.ext.todo",
    "nbsphinx",
]
# Todo
todo_include_todos = True


# Autosummary:
autodoc_default_flags = ["members"]
autosummary_generate = True

# Make sure the target is unique
autosectionlabel_prefix_document = True

# Improve typehints (default is signature)
autodoc_typehints = "description"

# Intersphinx
intersphinx_mapping = {
    "py-epicsarchiver": (
        "http://ics-infrastructure.pages.esss.lu.se/py-epicsarchiver",
        None,
    ),
    "python": ("https://docs.python.org/3", None),
    "numpy": ("https://numpy.org/doc/stable", None),
    "pandas": ("http://pandas.pydata.org/pandas-docs/dev", None),
}


extlinks = {
    "archiver": (
        "https://slacmshankar.github.io/epicsarchiver_docs/%s",
        "EPICS Archiver Appliance/%s",
    ),
    "archiver_mgmt_url": (
        "https://slacmshankar.github.io/epicsarchiver_docs/api/org/epics/archiverappliance/mgmt/bpl/%s",
        "Management URL/%s",
    ),
}


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns: list[str] = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]
